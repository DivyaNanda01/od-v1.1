'use strict';

angular.module('webApp', ['LocalStorageModule',
    'ui.bootstrap', // for modal dialogs
    'ngResource', 'ui.router', 'ngCookies','toasty', 'ngAria', 'ngCacheBuster', 'infinite-scroll','ngMaterial'])

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider,  httpRequestInterceptorCacheBusterProvider) {

    	$urlRouterProvider.otherwise('/login');
        $stateProvider.state('site', {
            'abstract': false,
            views: {
                'navbar@': {
                    templateUrl: 'scripts/components/navbar/navbar.html',
                    controller: 'NavbarController'
                },
                'leftside@': {
                    templateUrl: 'scripts/components/nav_panel/nav_panel.html',
                    controller: 'NavPanelController'
                }
            },
            resolve: {

            }
        });

     httpRequestInterceptorCacheBusterProvider.setMatchlist([/.*DataSource.*/,/.*users.*/],true);
        

    })

    .run(function($rootScope, $location, $cookieStore, $http) {

    'use strict';
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/login','/roleBasedAccess']) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
        }
    });
});

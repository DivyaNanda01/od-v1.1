'use strict';

angular.module('webApp')
    .controller('LoginController', function ($rootScope, $scope, $state, $timeout,AuthenticationService,RoleBasedService ) {
        $scope.user = {};
        $scope.errors = {};
        $scope.authenticationError = false;
        $scope.rememberMe = true;
        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        
        $scope.login = function (event) {
               
             event.preventDefault();
            AuthenticationService.Login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe
            },function(response){
            
                if(response.userName==undefined){
                    $scope.authenticationError = true;
                }else{
                	console.log(response)
                    AuthenticationService.SetCredentials(response.userName,response.createToken);
                    //RoleBasedService.setRole(response.dashBoardPermissionMap);
                    $state.go('dashboard');
                }

            });
        };
        
        AuthenticationService.ClearCredentials();
    });

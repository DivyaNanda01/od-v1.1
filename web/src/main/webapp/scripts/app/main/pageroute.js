'use strict';

angular.module('webApp')
    .config(function ($stateProvider) {

        $stateProvider
            .state('dashboard', {
                parent: 'site',
                url: '/dashboard',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/templates/main.html',
                        controller: 'MainController'

                    }
                },
                resolve: {

                }
            });

             $stateProvider
                        .state('transportFinance', {
                            parent: 'site',
                            url: '/transportFinance',
                            data: {
                                authorities: []
                            },
                            views: {
                                'content@': {
                                    templateUrl: 'scripts/app/main/templates/transportFinance.html',
                                    controller: 'transportFinanceCtrl'

                                }
                            },
                            resolve: {

                            }
                        });
                     $stateProvider
                            .state('dcDashboardOne', {
                                parent: 'site',
                                url: '/dcDashboardOne',
                                data: {
                                    authorities: []
                                },
                                views: {
                                    'content@': {
                                        templateUrl: 'scripts/app/main/templates/dcDashboardOne.html',
                                        controller: 'dcDashboardOneCtrl'

                                    }
                                },
                                resolve: {

                                }
                            });
                            $stateProvider
                                    .state('dcDashboardTwo', {
                                        parent: 'site',
                                        url: '/dcDashboardTwo',
                                        data: {
                                            authorities: []
                                        },
                                        views: {
                                            'content@': {
                                                templateUrl: 'scripts/app/main/templates/dcDashboardTwo.html',
                                                controller: 'dcDashboardTwoCtrl'

                                            }
                                        },
                                        resolve: {

                                        }
                                    });

            $stateProvider
            .state('confDashBoard', {
                parent: 'site',
                url: '/confDashBoard',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/templates/ConfDashboard.html',
                        controller: 'ConfiguredAirlineController'

                    }
                },
                resolve: {

                }
            });
        $stateProvider
        .state('layouts', {
            parent: 'site',
            url: '/layouts',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/main/templates/layouts.html',
                    controller: 'LayoutController'

                }
            },
            resolve: {

            }
        });


        
        $stateProvider
            .state('roleBasedAccess', {
                parent: 'site',
                url: '/roleBasedAccess',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/templates/roleBasedAccess.html',
                        controller: 'RoleBasedAccessController'

                    }
                },
                resolve: {

                }
            });
        $stateProvider
            .state('themeSettings', {
                parent: 'site',
                url: '/themeSettings',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/templates/themeSettings.html',
                        controller: 'ThemeSettingsController'

                    }
                },
                resolve: {

                }
            });




    });

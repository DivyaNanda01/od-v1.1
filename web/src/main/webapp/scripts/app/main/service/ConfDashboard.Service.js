'use strict';

angular.module('webApp').service('ConfiguredAirlineService', function($rootScope,$http,$q,$log,$location){
	
	
    this.getAirlineData =function(data){
        var deffered= $q.defer();
        $http({method:'GET',url:'api/rest/getData',headers:data})
            .success(function(data,status,header,config){
            	deffered.resolve(data);

            })
            .error(function(data,status,header,config){
            		if(status==403)
            			 $location.path('/login');
            		
            		deffered.reject(status,data);
            });
        return deffered.promise;
    }

  
    this.getUserAccessRolePermission = function(tkn,username){

    	var UserRoleAccess = $q.defer();
    	$http({method:'GET',url:'/api/admin/rest/user/permission/'+username,headers:tkn})
    		.success(function(data,status,header,config){
    			UserRoleAccess.resolve(data);    			
    		})
    		.error(function(data,status,header,config){
    			UserRoleAccess.reject(data,status)
    			
    		});
    		return UserRoleAccess.promise;    		
    }
    
    

});
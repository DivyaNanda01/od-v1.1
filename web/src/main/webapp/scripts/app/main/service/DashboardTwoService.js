'use strict';

angular.module('webApp').service('DashboardTwoService', function($rootScope,$http,$q,$log){

	var sinarmasData=[{"petak_id":"JAMBI","harvesting_completed":"DA","start_date":"JAMBI","end_date":"BKB0003000","act_wood_potency":"20151002","trgt_wood_potency":"1","total_cost":"30/04/2015","contractor":"29/06/2015","type_of_work":"76.98","duration":"50.89","tonnage":"147148872","avg_cost_per_tonnage":"90008910","wood_potency_diff":"Kebersihan Lahan Flat;Slashing (Kebersihan Lahan);Tebang;Loading to Truck HTI;Extraction"},{"petak_id":"JAMBI","harvesting_completed":"DA","start_date":"JAMBI","end_date":"BKB0003001","act_wood_potency":"20151002","trgt_wood_potency":"1","total_cost":"07/05/2015","contractor":"06/07/2015","type_of_work":"77.89","duration":"95.71","tonnage":"141099426","avg_cost_per_tonnage":"90008910","wood_potency_diff":"Kebersihan Lahan;Slashing (Kebersihan Lahan);Tebang;Loading to Truck HTI;Extraction"},{"petak_id":"JAMBI","harvesting_completed":"DA","start_date":"JAMBI","end_date":"TPH0005000","act_wood_potency":"20151002","trgt_wood_potency":"1","total_cost":"12/05/2015","contractor":"11/07/2015","type_of_work":"176.92","duration":"181.477","tonnage":"234350303","avg_cost_per_tonnage":"90001569","wood_potency_diff":"Tebang;Extraction;Loading to Truck HTI;Kebersihan Lahan;Slashing (Kebersihan Lahan)"},{"petak_id":"JAMBI","harvesting_completed":"DA","start_date":"JAMBI","end_date":"TPH0004701","act_wood_potency":"20151002","trgt_wood_potency":"0","total_cost":"12/10/2015","contractor":"11/12/2015","type_of_work":"203.85","duration":"197.47","tonnage":"348853923","avg_cost_per_tonnage":"90001569","wood_potency_diff":"Tebang;Extraction;Loading to Sampan Besi /dgn Estafet HTI;Loading to Truck HTI;Kebersihan Lahan;Slashing (Kebersihan Lahan)"},{"petak_id":"JAMBI","harvesting_completed":"DA","start_date":"JAMBI","end_date":"TPH0027403","act_wood_potency":"20151002","trgt_wood_potency":"0","total_cost":"26/09/2015","contractor":"25/11/2015","type_of_work":"59.18","duration":"74.88","tonnage":"91918807","avg_cost_per_tonnage":"90008588","wood_potency_diff":"Tebang;Extraction;Loading to Truck HTI;Kebersihan Lahan;Slashing (Kebersihan Lahan)"}];


    this.getSinarmasData = function(data){

        var deffered= $q.defer();

        $http({method:'GET',url:'api/rest/getData',headers:data})
            .success(function(data,status,header,config){

            	deffered.resolve(data);

            })
            .error(function(data,status,header,config){
            		console.log(data);
            		//deferred.reject(status,data);
            });
        return deffered.promise;
    }

});
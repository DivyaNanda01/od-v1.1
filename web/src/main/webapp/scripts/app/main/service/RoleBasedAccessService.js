'use strict';

angular.module('webApp').service('RoleBasedService', function($http,$q,$log,toasty,$location){
	
	 function successToasty(message){
	   	 toasty.pop.success({
	         title: 'Success',
	         msg: message,
	         timeout: 1500,
	         showClose: true,
	         clickToClose: true
	     });
    }
    
    function errorToasty(message){
	   	 toasty.pop.error({
	         title: 'Error',
	         msg: message,
	         timeout: 1500,
	         showClose: true,
	         clickToClose: true
	     });
   }
    
    
    
    function checkInvalidSession(status){
    	if(status==403)
			 $location.path('/login');	
    }
    
    
    
    
    
  //Permission & Role Mapping to User
    
    this.getUserAccessRolePermission = function(tkn,username){
    	console.log(tkn);
    	console.log(username);
    	var UserRoleAccess = $q.defer();
    	$http({method:'GET',url:'/api/admin/rest/user/permission/'+username,headers:tkn})
    		.success(function(data,status,header,config){
    			UserRoleAccess.resolve(data);    			
    		})
    		.error(function(data,status,header,config){
    			checkInvalidSession(status);
    			UserRoleAccess.reject(data,status)
    			
    		});
    		return UserRoleAccess.promise;    		
    }
    
    
    
    
    
    //All Permission Based Service Call
    
    this.getAllPermissionObjects = function(tkn){
    	var PermissionObjects = $q.defer();
    	$http({method:'GET',url:'/api/admin/rest/objects',headers:tkn})
    		.success(function(data,status,header,config){
    			PermissionObjects.resolve(data);
    			console.log(data);
    		})
    		.error(function(data,status,header,config){
    			checkInvalidSession(status);
    			PermissionObjects.reject(data,status)
    			
    		});
    		return PermissionObjects.promise;
    		
    }
    
    this.getObjectPermissionsByRoleName = function(RoleName,tkn){
    	var PermissionByRole = $q.defer();
    	$http({method:'GET',url:'/api/admin/rest/permissions/'+RoleName,headers:tkn})
    		.success(function(data,status,header,config){
    			PermissionByRole.resolve(data);
    			
    		})
    		.error(function(data,status,header,config){
    			checkInvalidSession(status);
    			PermissionByRole.reject(data,status)
    		});
    		return PermissionByRole.promise;
    }
    
    
    this.saveEditedRolePermissions =function(RolePermissionData,tkn){
    	var NewPermissions = $q.defer();
    	
    	$http({method:'PUT',url:'/api/admin/rest/permissions',headers:tkn,data:RolePermissionData})
    		.success(function(data,status,header,config){
    			console.log(status);
    			console.log("yo")
    			successToasty('Updated Permissions Successfully!!');
    			NewPermissions.resolve(data);
    		})
    		.error(function(data,status){
    			console.log(status);
    			console.log(data);
    			errorToasty('Error Updating Permissions !!');
    			checkInvalidSession(status);
    			NewPermissions.reject(data,status);
    		});
    		return NewPermissions.promise;
    }
    
    
    
    
    
    
    
    
    
    
    //All Role Service Functions
    
    this.getAllRoles = function(tkn){
    	var roledeffered = $q.defer();
    	$http({method:'GET',url:'/api/admin/rest/roles/',headers:tkn})
    		.success(function(data,status,header,config){
    			roledeffered.resolve(data);
    			console.log(data);
    		})
    		.error(function(data,status,header,config){
    			roledeffered.reject(data,status);
    			checkInvalidSession(status);
    		});
    		return roledeffered.promise;
    	}
    	this.saveNewRole = function(RoleData,tkn){
    		console.log("Add New Role");
    		console.log(RoleData);
    		var defer = $q.defer();
        	$http({method:'POST',url:'/api/admin/rest/role',headers:tkn,data:RoleData})
        	.success(function(data,status,header,config){
        		successToasty('Role Added Successfully!!');
        		defer.resolve(data,status);
        	})
        	.error(function(data,status){
        		errorToasty('Failed to Add Role!!');
        		checkInvalidSession(status);
        		defer.reject(data,status);
        	});
        	return defer.promise;
    	}
    	
	    this.saveEditedRole = function(RoleData,tkn){
	    	console.log("Edit Role");
    		console.log(RoleData);
	    	var defer = $q.defer();
	    	$http({method:'PUT',url:'/api/admin/rest/role',headers:tkn,data:RoleData})
	    	.success(function(data,status,header,config){
	    		successToasty('Role Updated Successfully!!');
	    		defer.resolve(data,status);
	    	})
	    	.error(function(data,status){
	    		checkInvalidSession(status);
	    		errorToasty('Failed to Update Role!!');
	    		defer.reject(data,status);
	    	});
	    	return defer.promise;
	    }
	    
	    this.deleteSelectedRole = function(rolename,tkn){   
	    	console.log("test")
	    	var defer = $q.defer();
	    	$http({method:'DELETE',url:'/api/admin/rest/role',headers:tkn,data:rolename})
	    		.success(function(data,status,header,config){
	    		 successToasty('Role Deleted!!');
	    		 defer.resolve(data);
	    	})
	    		.error(function(data,status){
	    			console.log(data);
	    			checkInvalidSession(status);
	    			errorToasty('Unable to Delete Role!!');
	    			defer.reject(data,status);
	    	});
	    	return defer.promise;
	    }
    
    
    
    
    
    
    
    
    
    
    
    
   
    //All User Service Functions
    this.getAllUsers = function(tkn){
    	
    	var defferred = $q.defer();
    	
    	$http({method:'GET',url:'/api/admin/rest/users/',headers:tkn})
    	.success(function(data,status,header,config){
    		defferred.resolve(data);
    		
    	})
    	.error(function(data,status){
    		checkInvalidSession(status);
    			defferred.reject(data,status);
    	});
    	return defferred.promise;
    	
    }
    
    this.saveNewUser = function(Userdata,tkn){
    	
    	var defer = $q.defer();
    	$http({method:'POST',url:'/api/admin/rest/user',headers:tkn,data:Userdata})
    	.success(function(data,status,header,config){
    		successToasty('New User Created!!');
    		defer.resolve(data);
    	})
    	.error(function(data,status){
    		checkInvalidSession(status);
    		errorToasty('Unable to Create New User!!');
    		defer.reject(data,status)
    	});
    	return defer.promise;
    }
    
    this.saveEditedUser = function(UserData,tkn){
    	var defer = $q.defer();
    	$http({method:'PUT',url:'/api/admin/rest/user',headers:tkn,data:UserData})
    	.success(function(data,status,header,config){
    		successToasty('User Details Updated!!');
    		defer.resolve(data,status);
    	})
    	.error(function(data,status){
    		checkInvalidSession(status);
    		errorToasty('Failed to Update!!');
    		defer.reject(data,status)
    	});
    	return defer.promise;
    }
    this.deleteSelectedUser = function(userName,tkn){
    	console.log(userName)
    	var defer = $q.defer();
    	$http({method:'DELETE',url:'/api/admin/rest/user',headers:tkn,data:userName})
    	.success(function(data,status,header,config){
    		 successToasty('User Deleted!!');
    		defer.resolve(data);
    	})
    	.error(function(data,status){
    		checkInvalidSession(status);
    		errorToasty('Unable to Delete!!');
    		defer.reject(data,status);
    	});
    	return defer.promise;
    }

});



'use strict';
angular.module('webApp')
    .controller('transportFinanceCtrl', function ($scope, DataSource,$filter,$rootScope) {

        $scope.currencies = ['USD','EURO','RUPEE'];
         $scope.volumeType = ['Carrier Type','Channel'];
        $scope.volumeView = ['Cartons', 'Orders'];
        $scope.viewAs = ['Percentage', 'Total'];

        $scope.myDate = new Date();
          $scope.minDate = new Date(
              $scope.myDate.getFullYear(),
              $scope.myDate.getMonth() - 2,
              $scope.myDate.getDate());
          $scope.maxDate = new Date(
              $scope.myDate.getFullYear(),
              $scope.myDate.getMonth() + 2,
              $scope.myDate.getDate());
          $scope.onlyWeekendsPredicate = function(date) {
            var day = date.getDay();
            return day === 0 || day === 6;
          }

        $scope.deliveryVolume = c3.generate({
            bindto: '#deliveryVolume',
            data: {
                columns: [
                    ['Network', 30, 200, 200, 400, 150, 250, 111, 442, 411, 333 , 222],
                    ['OwnFleet', 130, 100, 100, 200, 150, 50, 121, 433, 777, 232, 111],
                    ['ThirdParty', 230, 200, 200, 300, 250, 250, 555, 232, 111, 635 ]
                ],
                type: 'bar',
                groups: [
                    ['Network', 'OwnFleet', 'ThirdParty']
                ]
            },
             axis : {
                  rotated: true
             },
            grid: {
                y: {
                    lines: [{value:0}]
                }
            }
        });





});
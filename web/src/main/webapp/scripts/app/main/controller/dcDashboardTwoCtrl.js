'use strict';
angular.module('webApp')
    .controller('dcDashboardTwoCtrl', function ($scope, DashboardTwoService,$filter,$rootScope) {

        $scope.currencies = ['$ USD','€ EURO','£ GBP'];
        $scope.viewAs = [ 'Total Sales', 'Goods Sold'];
        $scope.myDate = new Date();
                        $scope.minDate = new Date(
                            $scope.myDate.getFullYear(),
                            $scope.myDate.getMonth() - 2,
                            $scope.myDate.getDate());
                        $scope.maxDate = new Date(
                            $scope.myDate.getFullYear(),
                            $scope.myDate.getMonth() + 2,
                            $scope.myDate.getDate());
                        $scope.onlyWeekendsPredicate = function(date) {
                          var day = date.getDay();
                          return day === 0 || day === 6;
                        }

       $scope.sites = ['Manchester','Dublin', 'Prague','GOH','Leicester','Lenzburg','Madrid','MSL','Senlis','Siziano','SMC','Strangnas','Zwolle'];

       var tkn=$rootScope.globals.currentUser.authdata;

        //Dashboard two: for dc_db_l3_measure
        var sinarmasHeader3 = {
                             'username': 'root',
                             'password': 'redhat',
                             'queryname': 'dc_db_l3_measuretablequery',
                             'x-auth-token':tkn,
                             'filterconditions': '1=1'
                     }

                     DashboardTwoService.getSinarmasData(sinarmasHeader3).then(function (data) {
                             $scope.DcDbL3Measure = data;


                     });


             //Dashboard two: for dc_db_l3_fin
                 var sinarmasHeader4 = {
                                      'username': 'root',
                                      'password': 'redhat',
                                      'queryname': 'dc_db_l3_fintablequery ',
                                      'x-auth-token':tkn,
                                      'filterconditions': '1=1'
                              }

                              DashboardTwoService.getSinarmasData(sinarmasHeader4).then(function (data) {
                                      $scope.DcDbL3FinTableResponse = data;
                              });


             //Dashboard two: for dc_db_l3_dept_dtl
                 var sinarmasHeader5 = {
                                      'username': 'root',
                                      'password': 'redhat',
                                      'queryname': 'dc_db_l3_dept_dtltablequery',
                                      'x-auth-token':tkn,
                                      'filterconditions': '1=1'
                              }

                              DashboardTwoService.getSinarmasData(sinarmasHeader5).then(function (data) {
                                      $scope.DcDbl3DeptResponse = data;
                              });

      $scope.chartAreaLine = c3.generate({
        bindto: '#chartAreaLine',
         data: {
          x:'x',
             columns: [
                 ['x','Jan','Feb', 'Mar', 'Apr', 'May','June'],
                 ['SiteProductivity', 30, 20, 50, 40, 60, 50],
                 ['Target', 100, 130, 90, 140, 10, 120]
             ],
             type: 'bar',
             types: {
                 SiteProductivity: 'line',
                 Target: 'area',
             }
         },
          axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: 75,
                    multiline: false
                }
            }
        }
     });

     $scope.dcOtac = c3.generate({
        bindto: '#dcOtac',
                         data: {
                         x:'x',
                             columns: [
                             ['x','Jan','Feb', 'Mar', 'Apr', 'May','June'],
                                 ['DcQuality', 30, 100, 60, 70, 50, 90],
                                 ['DcShipped', 50, 20, 10, 40, 15, 25],
                                 ['DcTarget', 20,30, 40, 60, 48,90]
                             ]
                         },
                         axis: {
                                      x: {
                                          type: 'category',
                                          tick: {
                                              rotate: 75,
                                              multiline: false
                                          }
                                      }
                                  }
                     });
});
'use strict';
angular.module('webApp')
    .controller('MainController', function ($scope, DataSource,$filter,$rootScope,$http) {

                $scope.channels = ["SMB/CNS", "Contract", "Retail", "Others", "All"];
                $scope.currencies = [ {code:1,name:'€ EURO'}, {code:2, name:'£ GBP'}, {code:3,name:'$ USD'},];
                $scope.volumeType = ['Carrier Type','Channel'];
                $scope.volumeView = ['Cartons', 'Orders'];
                $scope.viewAs = ['Percentage', 'Total'];
                $scope.metrics = ['Overall','Inventory','DC Shipped', 'DC Quality','Transport'];

                $scope.myDate = new Date();
                  $scope.minDate = new Date(
                      $scope.myDate.getFullYear(),
                      $scope.myDate.getMonth() - 2,
                      $scope.myDate.getDate());
                  $scope.maxDate = new Date(
                      $scope.myDate.getFullYear(),
                      $scope.myDate.getMonth() + 2,
                      $scope.myDate.getDate());
                  $scope.onlyWeekendsPredicate = function(date) {
                    var day = date.getDay();
                    return day === 0 || day === 6;
                  }

                  $scope.update = function() {
                     $scope.item.currency.code = $scope.selectedItem.code;
                  }

                  $scope.transportVolumeSummary =  c3.generate({
                                        bindto: '#transportVolumeSummary',
                                        data: {
                                            x:'x',
                                           columns: [
                                               ['x','Sweden','Spain','Germany','CzechRep','France', 'Germany', 'Italy', 'Netherlands','Switzerland','UK & IE'],
                                               ['Network', 30, 102, 100, 110, 150, 110,70,55,88,120,134],
                                               ['Own_Fleet', 50, 20, 10, 40, 15, 25 ,56, 34,78, 90,111],
                                               ['DVO_3P', 110, 100, 110, 140, 115, 125, 77,88,123,99,120],
                                               ['Cust_3P', 110, 70, 90, 120, 165, 105,99,129,56,76,55]
                                           ],

                                           types: {
                                               Network: 'bar',
                                               Own_Fleet: 'bar',
                                               DVO_3P: 'bar',
                                               Cust_3P:'bar'
                                           }
                                        },
                                          legend: {
                                                show: true,
                                                position: 'inset',
                                                inset: {
                                                    anchor: 'top-right',
                                                   x:undefined,
                                                               y: undefined,
                                                               step: undefined
                                                }
                                            },
                                          axis: {
                                          rotated:true,
                                                 x: {
                                                     type: 'category',
                                                     tick: {
                                                         rotate: 75,
                                                         multiline: false
                                                     },
                                                     height: 130
                                                 }
                                             }
                                    });
                                     setTimeout(function () {
                                                        $scope.transportVolumeSummary.resize({
                                                       // height:angular.element('.C3ReswaraTotalCostDaily').innerHeight(),
                                                       // width:300
                                                        })
                                                    }, 1000);

                var tkn=$rootScope.globals.currentUser.authdata;

                var sinarmasHeader = {
                		'username': 'root', 
                		'password': 'redhat', 
                		'queryname': 'Executivetablequery ',
                		'x-auth-token':tkn,
                		'filterconditions': '1=1'
                		}

                DataSource.getSinarmasData(sinarmasHeader).then(function (data) {
                        $scope.ExecDashData = data;
                          $scope.initDataFormat = function(){
                                 appendColors($scope.ExecDashData); //get color codes
                                 initThousandSeperator($scope.ExecDashData); //implement 1000 seperator
                             }

                        // $scope.dataToRender = appendColors($scope.ExecDashData);

                });

//                $scope.ExecDashData =
//                [
//                   {
//                      "executive.dc_prod_region":"SWEDEN",
//                      "executive.dc_prod_orders":8000,
//                      "executive.dc_prod_ord_lns":32000,
//                      "executive.dc_prod_pck_lns":32000,
//                      "executive.dc_prod_lpmh":99,
//                      "executive.dc_fin_sales":20000,
//                      "executive.dc_fin_cost":10000,
//                      "executive.dc_fin_percent_sales":99,
//                      "executive.dc_fin_order":8,
//                      "executive.dc_fin_line":8,
//                      "executive.trans_fin_cost":10000,
//                      "executive.trans_fin_sales":99,
//                      "executive.trans_fin_order":9,
//                      "executive.trans_fin_crtn":6,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"SPAIN",
//                      "executive.dc_prod_orders":9000,
//                      "executive.dc_prod_ord_lns":40000,
//                      "executive.dc_prod_pck_lns":40000,
//                      "executive.dc_prod_lpmh":98,
//                      "executive.dc_fin_sales":23000,
//                      "executive.dc_fin_cost":13000,
//                      "executive.dc_fin_percent_sales":99,
//                      "executive.dc_fin_order":8,
//                      "executive.dc_fin_line":8,
//                      "executive.trans_fin_cost":14000,
//                      "executive.trans_fin_sales":99,
//                      "executive.trans_fin_order":7,
//                      "executive.trans_fin_crtn":8,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"GERMANY",
//                      "executive.dc_prod_orders":10000,
//                      "executive.dc_prod_ord_lns":40000,
//                      "executive.dc_prod_pck_lns":40000,
//                      "executive.dc_prod_lpmh":99,
//                      "executive.dc_fin_sales":26000,
//                      "executive.dc_fin_cost":16000,
//                      "executive.dc_fin_percent_sales":94,
//                      "executive.dc_fin_order":9,
//                      "executive.dc_fin_line":9,
//                      "executive.trans_fin_cost":18000,
//                      "executive.trans_fin_sales":94,
//                      "executive.trans_fin_order":9,
//                      "executive.trans_fin_crtn":7,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"CZECH REP",
//                      "executive.dc_prod_orders":11000,
//                      "executive.dc_prod_ord_lns":44000,
//                      "executive.dc_prod_pck_lns":44000,
//                      "executive.dc_prod_lpmh":97,
//                      "executive.dc_fin_sales":29000,
//                      "executive.dc_fin_cost":19000,
//                      "executive.dc_fin_percent_sales":97,
//                      "executive.dc_fin_order":6,
//                      "executive.dc_fin_line":6,
//                      "executive.trans_fin_cost":22000,
//                      "executive.trans_fin_sales":97,
//                      "executive.trans_fin_order":6,
//                      "executive.trans_fin_crtn":9,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"FRANCE",
//                      "executive.dc_prod_orders":12000,
//                      "executive.dc_prod_ord_lns":48000,
//                      "executive.dc_prod_pck_lns":48000,
//                      "executive.dc_prod_lpmh":96,
//                      "executive.dc_fin_sales":32000,
//                      "executive.dc_fin_cost":22000,
//                      "executive.dc_fin_percent_sales":96,
//                      "executive.dc_fin_order":7,
//                      "executive.dc_fin_line":7,
//                      "executive.trans_fin_cost":26000,
//                      "executive.trans_fin_sales":96,
//                      "executive.trans_fin_order":7,
//                      "executive.trans_fin_crtn":7,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"ITALY",
//                      "executive.dc_prod_orders":13000,
//                      "executive.dc_prod_ord_lns":52000,
//                      "executive.dc_prod_pck_lns":52000,
//                      "executive.dc_prod_lpmh":88,
//                      "executive.dc_fin_sales":35000,
//                      "executive.dc_fin_cost":25000,
//                      "executive.dc_fin_percent_sales":93,
//                      "executive.dc_fin_order":8,
//                      "executive.dc_fin_line":8,
//                      "executive.trans_fin_cost":30000,
//                      "executive.trans_fin_sales":93,
//                      "executive.trans_fin_order":8,
//                      "executive.trans_fin_crtn":8,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"NETHERLANDS",
//                      "executive.dc_prod_orders":14000,
//                      "executive.dc_prod_ord_lns":56000,
//                      "executive.dc_prod_pck_lns":56000,
//                      "executive.dc_prod_lpmh":98,
//                      "executive.dc_fin_sales":38000,
//                      "executive.dc_fin_cost":28000,
//                      "executive.dc_fin_percent_sales":87,
//                      "executive.dc_fin_order":6,
//                      "executive.dc_fin_line":6,
//                      "executive.trans_fin_cost":34000,
//                      "executive.trans_fin_sales":87,
//                      "executive.trans_fin_order":6,
//                      "executive.trans_fin_crtn":6,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"SWIZERLAND",
//                      "executive.dc_prod_orders":15000,
//                      "executive.dc_prod_ord_lns":60000,
//                      "executive.dc_prod_pck_lns":60000,
//                      "executive.dc_prod_lpmh":95,
//                      "executive.dc_fin_sales":41000,
//                      "executive.dc_fin_cost":31000,
//                      "executive.dc_fin_percent_sales":95,
//                      "executive.dc_fin_order":7,
//                      "executive.dc_fin_line":7,
//                      "executive.trans_fin_cost":38000,
//                      "executive.trans_fin_sales":95,
//                      "executive.trans_fin_order":7,
//                      "executive.trans_fin_crtn":7,
//                      "executive.report_date":14062016
//                   },
//                   {
//                      "executive.dc_prod_region":"UK & IE",
//                      "executive.dc_prod_orders":16000,
//                      "executive.dc_prod_ord_lns":64000,
//                      "executive.dc_prod_pck_lns":64000,
//                      "executive.dc_prod_lpmh":95,
//                      "executive.dc_fin_sales":44000,
//                      "executive.dc_fin_cost":34000,
//                      "executive.dc_fin_percent_sales":95,
//                      "executive.dc_fin_order":9,
//                      "executive.dc_fin_line":9,
//                      "executive.trans_fin_cost":42000,
//                      "executive.trans_fin_sales":95,
//                      "executive.trans_fin_order":9,
//                      "executive.trans_fin_crtn":9,
//                      "executive.report_date":14062016
//                   }
//                ];

        //MainDashboard - OtacInventoryTable
                var sinarmasHeader2 = {
                        'username': 'root',
                        'password': 'redhat',
                        'queryname': 'executive_otac_inventorytablequery ',
                        'x-auth-token':tkn,
                        'filterconditions': '1=1'
                }

                DataSource.getSinarmasData(sinarmasHeader2).then(function (data) {
                        $scope.OtacInventoryTableResponse = data;
                            $scope.initDataFormat2 = function(){
                                    appendColors($scope.OtacInventoryTableResponse); //get color codes
                                    initThousandSeperator2($scope.OtacInventoryTableResponse);
                                }

                        //$scope.dataToRender1 = appendColors($scope.OtacInventoryTableResponse);
                });

//    $scope.OtacInventoryTableResponse = [
//                                           {
//                                              "executive_otac_inventory.region":"CZECH REP",
//                                              "executive_otac_inventory.orders":11000,
//                                              "executive_otac_inventory.otac_per":97.0,
//                                              "executive_otac_inventory.invt_lfr_per":99.0,
//                                              "executive_otac_inventory.invt_turns":9.0,
//                                              "executive_otac_inventory.invt_st_val":2500000,
//                                              "executive_otac_inventory.invt_bo_val":99
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"NETHERLANDS",
//                                              "executive_otac_inventory.orders":15000,
//                                              "executive_otac_inventory.otac_per":98.0,
//                                              "executive_otac_inventory.invt_lfr_per":97.0,
//                                              "executive_otac_inventory.invt_turns":8.0,
//                                              "executive_otac_inventory.invt_st_val":2700000,
//                                              "executive_otac_inventory.invt_bo_val":97
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"GERMANY",
//                                              "executive_otac_inventory.orders":10000,
//                                              "executive_otac_inventory.otac_per":99.0,
//                                              "executive_otac_inventory.invt_lfr_per":98.0,
//                                              "executive_otac_inventory.invt_turns":9.0,
//                                              "executive_otac_inventory.invt_st_val":1800000,
//                                              "executive_otac_inventory.invt_bo_val":98
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"UK & IE",
//                                              "executive_otac_inventory.orders":20000,
//                                              "executive_otac_inventory.otac_per":95.0,
//                                              "executive_otac_inventory.invt_lfr_per":95.0,
//                                              "executive_otac_inventory.invt_turns":8.5,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":95
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"SPAIN",
//                                              "executive_otac_inventory.orders":9000,
//                                              "executive_otac_inventory.otac_per":98.0,
//                                              "executive_otac_inventory.invt_lfr_per":98.0,
//                                              "executive_otac_inventory.invt_turns":8.0,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":98
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"FRANCE",
//                                              "executive_otac_inventory.orders":14000,
//                                              "executive_otac_inventory.otac_per":96.0,
//                                              "executive_otac_inventory.invt_lfr_per":96.0,
//                                              "executive_otac_inventory.invt_turns":8.0,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":96
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"SWIZERLAND",
//                                              "executive_otac_inventory.orders":17000,
//                                              "executive_otac_inventory.otac_per":95.0,
//                                              "executive_otac_inventory.invt_lfr_per":95.0,
//                                              "executive_otac_inventory.invt_turns":9.0,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":95
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"SWEDEN",
//                                              "executive_otac_inventory.orders":8000,
//                                              "executive_otac_inventory.otac_per":99.0,
//                                              "executive_otac_inventory.invt_lfr_per":99.0,
//                                              "executive_otac_inventory.invt_turns":6.0,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":99
//                                           },
//                                           {
//                                              "executive_otac_inventory.region":"ITALY",
//                                              "executive_otac_inventory.orders":14000,
//                                              "executive_otac_inventory.otac_per":88.0,
//                                              "executive_otac_inventory.invt_lfr_per":88.0,
//                                              "executive_otac_inventory.invt_turns":7.0,
//                                              "executive_otac_inventory.invt_st_val":2000000,
//                                              "executive_otac_inventory.invt_bo_val":88
//                                           }
//                                        ];

      // $scope.dataToRender1 = appendColors($scope.OtacInventoryTableResponse);


          //Main Dashboard - Aggregated Otac Inventory
                          var sinarmasHeader6 = {
                                  'username': 'root',
                                  'password': 'redhat',
                                  'queryname': 'executive_otac_inventory_perctablequery',
                                  'x-auth-token':tkn,
                                  'filterconditions': '1=1'
                          }

                          DataSource.getSinarmasData(sinarmasHeader6).then(function (data) {
                                  $scope.OtacInventoryTableAggregateResponse = data;
                                    $scope.initDataFormat1 = function(){
                                          appendColors($scope.OtacInventoryTableAggregateResponse); //get color codes
                                          initThousandSeperator1($scope.OtacInventoryTableAggregateResponse)
                                      }

                                  //$scope.dataToRender2 = appendColors($scope.OtacInventoryTableAggregateResponse);

                          });

//                          $scope.OtacInventoryTableAggregateResponse =[
//                                 {
//                                    "executive_otac_inventory_perc.otac":96,
//                                    "executive_otac_inventory_perc.inventory":99,
//                                    "executive_otac_inventory_perc.dc_shipment":95,
//                                    "executive_otac_inventory_perc.dc_quality":97,
//                                    "executive_otac_inventory_perc.transport":99,
//                                    "executive_otac_inventory_perc.lfr_perc":96,
//                                    "executive_otac_inventory_perc.stock_turns":10,
//                                    "executive_otac_inventory_perc.stock_value":2300000,
//                                    "executive_otac_inventory_perc.b_o_value":1240
//                                 }
//                              ];
                           //   $scope.dataToRender2 = appendColors($scope.OtacInventoryTableAggregateResponse);



//    $scope.initDataFormat = function(){
//        appendColors($scope.ExecDashData); //get color codes
//        initThousandSeperator($scope.ExecDashData); //implement 1000 seperator
//    }

    function initThousandSeperator(data){
         for (var index = 0; index < data.length; index++) {
                var item = data[index];
                item.dc_prod_ordersFormat = item["executive.dc_prod_orders"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.dc_prod_ord_lnsFormat = item["executive.dc_prod_ord_lns"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.dc_prod_pck_lnsFormat = item["executive.dc_prod_pck_lns"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.dc_fin_salesFormat = item["executive.dc_fin_sales"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.dc_fin_costFormat = item["executive.dc_fin_cost"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.trans_fin_costFormat = item["executive.trans_fin_cost"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
    }


//     $scope.initDataFormat1 = function(){
//        appendColors($scope.OtacInventoryTableAggregateResponse); //get color codes
//        initThousandSeperator1($scope.OtacInventoryTableAggregateResponse)
//    }

    function initThousandSeperator1(data){
         for (var index = 0; index < data.length; index++) {
                var item = data[index];
                item.stock_valueFormat = item["executive_otac_inventory_perc.stock_value"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
    }

//    $scope.initDataFormat2 = function(){
//            appendColors($scope.OtacInventoryTableResponse); //get color codes
//            initThousandSeperator2($scope.OtacInventoryTableResponse);
//        }


    function initThousandSeperator2(data){
         for (var index = 0; index < data.length; index++) {
                var item = data[index];
                item.ordersFormat = item["executive_otac_inventory.orders"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                item.invt_st_valFormat = item["executive_otac_inventory.invt_st_val"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              }
    }

                //Color Code Values
                                  function appendColors(data) {
                                    var mc = {
                                       '0-93': 'ODred',
                                       '94-95': 'ODorange',
                                       '96-100': 'ODgreen'
                                    };

                                    var min;
                                    var max;

                                    // Loop over 'data' and push the corresponding color property.
                                    for(var index = 0; index < data.length; index++) {
                                      var item = data[index];

                                      // Check item.data1 lies in which of the ranges present in 'mc'
                                      for (var key in mc) {
                                        // skip loop if the property is from prototype
                                        if (!mc.hasOwnProperty(key)) continue;

                                        // Now spilt the key to get the min and max
                                        var numbers = key.split('-');
                                        min = parseInt(numbers[0], 10);
                                        max = parseInt(numbers[1], 10);

                                        if(between(item["executive.dc_prod_lpmh"], min, max)) {
//                                         item["executive.dc_prod_lpmh"] = item["executive.dc_prod_lpmh"].toFixed(2);
                                          var color = mc[key];
                                          item.dc_prod_lpmhColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory.otac_per"], min, max)) {
                                          var color = mc[key];
                                          item.otac_perColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory.invt_lfr_per"], min, max)) {
                                          var color = mc[key];
                                          item.invt_lfr_perColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory_perc.otac"], min, max)) {
                                          var color = mc[key];
                                          item.otacColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory_perc.inventory"], min, max)) {
                                          var color = mc[key];
                                          item.inventoryColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory_perc.dc_shipment"], min, max)) {
                                          var color = mc[key];
                                          item.dc_shipmentColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory_perc.dc_quality"], min, max)) {
                                          var color = mc[key];
                                          item.dc_qualityColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["executive_otac_inventory_perc.transport"], min, max)) {
                                          var color = mc[key];
                                          item.transportColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }

                                      }
                                    }
                                  }

                                  function between(x, min, max) {
                                    return x >= min && x <= max;
                                  }
    });
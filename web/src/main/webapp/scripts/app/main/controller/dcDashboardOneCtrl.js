'use strict';
angular.module('webApp')
    .controller('dcDashboardOneCtrl', function ($scope, DashboardOneService,$filter,$rootScope,$http) {

 $scope.currencies = [ {code:1,name:'€ EURO'}, {code:2, name:'£ GBP'}, {code:3,name:'$ USD'},];        $scope.viewAs = [ 'Total Sales', 'Goods Sold'];
        $scope.myDate = new Date();
                        $scope.minDate = new Date(
                            $scope.myDate.getFullYear(),
                            $scope.myDate.getMonth() - 2,
                            $scope.myDate.getDate());
                        $scope.maxDate = new Date(
                            $scope.myDate.getFullYear(),
                            $scope.myDate.getMonth() + 2,
                            $scope.myDate.getDate());
                        $scope.onlyWeekendsPredicate = function(date) {
                          var day = date.getDay();
                          return day === 0 || day === 6;
                        }

        var tkn=$rootScope.globals.currentUser.authdata;

         var sinarmasHeader1 = {
                        'username': 'root',
                        'password': 'redhat',
                        'queryname': 'Detailstablequery ',
                        'x-auth-token':tkn,
                        'filterconditions': '1=1'
             }

                    DashboardOneService.getSinarmasData(sinarmasHeader1).then(function (data) {
                            $scope.dcDashboardOneData = data;
                            $scope.initDataFormat = function(){
                                 appendColors($scope.dcDashboardOneData); //get color codes
                                 initThousandSeperator($scope.dcDashboardOneData); //implement 1000 seperator
                          }
                    });


                     function initThousandSeperator(data){
                         for (var index = 0; index < data.length; index++) {
                            var item = data[index];
                            item.ordersFormat = item["details.orders"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.cartonsFormat = item["details.cartons"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.ord_lnsFormat = item["details.ord_lns"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.pk_lnsFormat = item["details.pk_lns"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.lpmhFormat = item["details.lpmh"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.costFormat = item["details.cost"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            item.salesFormat = item["details.sales"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                              }
                        }


                //Color Code Values
                                  function appendColors(data) {
                                    var mc = {
                                      '0-93': 'ODred',
                                      '94-95': 'ODorange',
                                      '96-100': 'ODgreen',
                                      '1000-5000': 'ODred',
                                      '5001-7000':'ODorange',
                                      '7001-10000':'ODgreen'
                                    };

                                    var min;
                                    var max;

                                    // Loop over 'data' and push the corresponding color property.
                                    for(var index = 0; index < data.length; index++) {
                                      var item = data[index];

                                      // Check item.data1 lies in which of the ranges present in 'mc'
                                      for (var key in mc) {
                                        // skip loop if the property is from prototype
                                        if (!mc.hasOwnProperty(key)) continue;

                                        // Now spilt the key to get the min and max
                                        var numbers = key.split('-');
                                        min = parseInt(numbers[0], 10);
                                        max = parseInt(numbers[1], 10);


                                        if(between(item["details.dc_ship"], min, max)) {
                                          var color = mc[key];
                                          item.dc_shipColor = color;
                                         // break; // We don't need to check for other ranges! done here.
                                        }
                                        if(between(item["details.dc_qlty"], min, max)) {
                                                  var color = mc[key];
                                                  item.dc_qltyColor = color;
                                                 // break; // We don't need to check for other ranges! done here.
                                        }

                                        if(between(item["details.lpmh"], min, max)) {
                                                  var color = mc[key];
                                                  item.lpmhColor = color;
                                                 // break; // We don't need to check for other ranges! done here.
                                        }


                                      }
                                    }
                                  }

                                  function between(x, min, max) {
                                    return x >= min && x <= max;
                                  }

});
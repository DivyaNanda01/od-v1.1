'use strict';
angular.module('webApp')
    .controller('ConfiguredAirlineController', function ($scope, ConfiguredAirlineService,RoleBasedService,$filter,$rootScope) {
               
                
                var tkn=$rootScope.globals.currentUser.authdata;
                var authToken={'x-auth-token':tkn};
                
                var username=$rootScope.globals.currentUser.username;
                var airlineHeader = {
                		'queryname': 'airlinequery', 
                		'x-auth-token':tkn,
                		'filterconditions': '1=1'
                		}
                
                $scope.airlineTableData=[];
                console.log(tkn)
                $scope.arrdelay=0;
                $scope.depdelay=0;
                var countByOrigin=[0,0,0,0,0,0];
                var delaybyOrigin=[0,0,0,0,0,0];
                var countByDest=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
                var delayByDest=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
                var flightTimeCountDeparture=[0,0,0,0];// morning,afternoon, eve,night flights count array
                var flightTimeCountArrival=[0,0,0,0];// morning,afternoon, eve,night flights count array - arrival side
      
                //Creating Permission Objects as per Role. These will be used to assign certain privilege to the user as they Login
                $scope.CanViewTable =false;
                $scope.CanExportTable=false;
                $scope.CanViewFlightTimingChart=false;
                $scope.CanExportFlightTimingChart=false;
                $scope.CanViewFreqentAirportChart=false;
                $scope.CanExportFreqentAirportChart=false;
                $scope.CanViewDelayChart=false;
                $scope.CanExportDelayChart=false;
                
      //Get Access for User
                RoleBasedService.getUserAccessRolePermission(authToken,username).then(function(data){
                	console.log("Inside Role Assessment");
                	console.log(data);
                	angular.forEach(data,function(maindata,key){
                		console.log(maindata.name);
                		
                		angular.forEach(maindata.permission,function(permissiondata,permissionkey){
                			
                			if(maindata.name == 'Airline_Table_Data'){
                				if(permissiondata.name=='read' && permissiondata.enabled == true)
                					$scope.CanViewTable =true;
                				if(permissiondata.name=='export' && permissiondata.enabled == true)
                					$scope.CanExportTable=true;
                            }
                			
                			if(maindata.name == 'Airline_Flight_Timing_Chart'){
                				if(permissiondata.name=='read' && permissiondata.enabled == true)
                					$scope.CanViewFlightTimingChart =true;
                				if(permissiondata.name=='export' && permissiondata.enabled == true)
                					$scope.CanExportFlightTimingChart=true;
                            }
                			
                			if(maindata.name == 'Airline_Frequent_Airport_Chart'){
                				if(permissiondata.name=='read' && permissiondata.enabled == true)
                					$scope.CanViewFreqentAirportChart =true;
                				if(permissiondata.name=='export' && permissiondata.enabled == true)
                					$scope.CanExportFreqentAirportChart=true;
                            }
                			
                			if(maindata.name == 'Airline_Delay_Chart'){
                				if(permissiondata.name=='read' && permissiondata.enabled == true)
                					$scope.CanViewDelayChart =true;
                				if(permissiondata.name=='export' && permissiondata.enabled == true)
                					$scope.CanExportDelayChart=true;
                            }
                				
                			
                		});
                	});
                });
       
          $scope.xx={};
        ConfiguredAirlineService.getAirlineData(airlineHeader).then(function (data) {
    	    $scope.xx=data;
        	angular.forEach(data,function(maindata,key){
            	$scope.arrdelay=$scope.arrdelay+maindata.arrdelay;
        		$scope.depdelay=$scope.arrdelay+maindata.depdelay;
        		
        		if(maindata.origin=='LAS') {countByOrigin[0]=countByOrigin[0]+1; delaybyOrigin[0]=delaybyOrigin[0]+maindata.depdelay;}
        		if(maindata.origin=='IND') {countByOrigin[1]=countByOrigin[1]+1; delaybyOrigin[1]=delaybyOrigin[1]+maindata.depdelay;}
        		if(maindata.origin=='IAD') {countByOrigin[2]=countByOrigin[2]+1; delaybyOrigin[2]=delaybyOrigin[2]+maindata.depdelay;}
        		if(maindata.origin=='ISP') {countByOrigin[3]=countByOrigin[3]+1; delaybyOrigin[3]=delaybyOrigin[3]+maindata.depdelay;}
        		if(maindata.origin=='JAN') {countByOrigin[4]=countByOrigin[4]+1; delaybyOrigin[4]=delaybyOrigin[4]+maindata.depdelay;}
        		if(maindata.origin=='JAX') {countByOrigin[5]=countByOrigin[5]+1; delaybyOrigin[5]=delaybyOrigin[5]+maindata.depdelay;}
        		
        		if(maindata.dest==	'TPA')	{countByDest[0]	=countByDest[0]	+1;delayByDest[0]=delayByDest[0]+maindata.arrdelay}
        		if(maindata.dest==	'BWI')	{countByDest[1]	=countByDest[1]	+1;delayByDest[1]=delayByDest[1]+maindata.arrdelay}
        		if(maindata.dest==	'JAX')	{countByDest[2]	=countByDest[2]	+1;delayByDest[2]=delayByDest[2]+maindata.arrdelay}
        		if(maindata.dest==	'LAS')	{countByDest[3]	=countByDest[3]	+1;delayByDest[3]=delayByDest[3]+maindata.arrdelay}
        		if(maindata.dest==	'MCI')	{countByDest[4]	=countByDest[4]	+1;delayByDest[4]=delayByDest[4]+maindata.arrdelay}
        		if(maindata.dest==	'MCO')	{countByDest[5]	=countByDest[5]	+1;delayByDest[5]=delayByDest[5]+maindata.arrdelay}
        		if(maindata.dest==	'MDW')	{countByDest[6]	=countByDest[6]	+1;delayByDest[6]=delayByDest[6]+maindata.arrdelay}
        		if(maindata.dest==	'PHX')	{countByDest[7]	=countByDest[7]	+1;delayByDest[7]=delayByDest[7]+maindata.arrdelay}
        		if(maindata.dest==	'FLL')	{countByDest[8]	=countByDest[8]	+1;delayByDest[8]=delayByDest[8]+maindata.arrdelay}
        		if(maindata.dest==	'PBI')	{countByDest[9]	=countByDest[9]	+1;delayByDest[9]=delayByDest[9]+maindata.arrdelay}
        		if(maindata.dest==	'RSW')	{countByDest[10]=countByDest[10]+1;delayByDest[10]=delayByDest[10]+maindata.arrdelay}
        		if(maindata.dest==	'HOU')	{countByDest[11]=countByDest[11]+1;delayByDest[11]=delayByDest[11]+maindata.arrdelay}
        		if(maindata.dest==	'BHM')	{countByDest[12]=countByDest[12]+1;delayByDest[12]=delayByDest[12]+maindata.arrdelay}
        		if(maindata.dest==	'BNA')	{countByDest[13]=countByDest[13]+1;delayByDest[13]=delayByDest[13]+maindata.arrdelay}
        		if(maindata.dest==	'JND')	{countByDest[14]=countByDest[14]+1;delayByDest[14]=delayByDest[14]+maindata.arrdelay}
        		if(maindata.dest==	'ORF')	{countByDest[15]=countByDest[15]+1;delayByDest[15]=delayByDest[15]+maindata.arrdelay}
        		if(maindata.dest==	'ABQ')	{countByDest[16]=countByDest[16]+1;delayByDest[16]=delayByDest[16]+maindata.arrdelay}
        		if(maindata.dest==	'ALM')	{countByDest[17]=countByDest[17]+1;delayByDest[17]=delayByDest[17]+maindata.arrdelay}
        		if(maindata.dest==	'AMA')	{countByDest[18]=countByDest[18]+1;delayByDest[18]=delayByDest[18]+maindata.arrdelay}
        		if(maindata.dest==	'AUS')	{countByDest[19]=countByDest[19]+1;delayByDest[19]=delayByDest[19]+maindata.arrdelay}
        		if(maindata.dest==	'BDL')	{countByDest[20]=countByDest[20]+1;delayByDest[20]=delayByDest[20]+maindata.arrdelay}
        		if(maindata.dest==	'BOI')	{countByDest[21]=countByDest[21]+1;delayByDest[21]=delayByDest[21]+maindata.arrdelay}

        		//Origin Flight Time Count
        		if(maindata.deptime<=600)
        			flightTimeCountDeparture[0]=flightTimeCountDeparture[0]+1;
        		else if(maindata.deptime >600 && maindata.deptime <=1200)
        			flightTimeCountDeparture[1]=flightTimeCountDeparture[1]+1;
        		else if(maindata.deptime >1200 && maindata.deptime <=1800)
        			flightTimeCountDeparture[2]=flightTimeCountDeparture[2]+1;
        		else if(maindata.deptime >1800 && maindata.deptime <=2400)
        			flightTimeCountDeparture[3]=flightTimeCountDeparture[3]+1;
        		
        		//Departure Flight time count
        		if(maindata.arrtime<=600)
        			flightTimeCountArrival[0]=flightTimeCountArrival[0]+1
        		else if(maindata.arrtime >600 && maindata.arrtime <=1200)
        			flightTimeCountArrival[1]=flightTimeCountArrival[1]+1
        		else if(maindata.arrtime >1200 && maindata.arrtime <=1800)
        			flightTimeCountArrival[2]=flightTimeCountArrival[2]+1
        		else if(maindata.arrtime >1800 && maindata.arrtime <=2400)
        			flightTimeCountArrival[3]=flightTimeCountArrival[3]+1
        			
        			var st,arr1;
        			
            	
            		if ((maindata.year == null) || (maindata.month == null) || (maindata.dayofmonth==null) || (maindata.dayofweek ==null) )
            				  		console.log("");
            		else{
            			st=(maindata.deptime+"");
            			if(st.length == 3)
            				st=(st.slice(0, 1) + ":" + st.slice(1,3));
            			if(st.length == 4)
            				st=(st.slice(0, 2) + ":" + st.slice(2,4));
            			
            			arr1=(maindata.rsarrtime+"");
            			if(arr1.length == 3)
            				arr1=(arr1.slice(0, 1) + ":" + arr1.slice(1,3));
            			if(arr1.length == 4)
            				arr1=(arr1.slice(0, 2) + ":" + arr1.slice(2,4));
            			
            			
            			$scope.airlineTableData.push({
            				'unique_carrier':maindata.uniquecarrier,
            				'tailnum':maindata.tailnum,
            				'flightnum':maindata.flightnum,
            				'year':maindata.year,
            				'month':maindata.month,
            				'day':maindata.dayofmonth,
            				'origin':maindata.origin,
            				'dest':maindata.dest,
            				'dist':maindata.distance,
            				'airtime':maindata.airtime,
            				'deptime':st,
            				'depdelay':maindata.depdelay,
            				'arrtime':arr1,
            				'arrdelay':maindata.arrdelay
            				
            			});
            		
            		}//end of else
            	 	
       }); //end of angular foreach
            
            /*console.log(countByOrigin)
            console.log(countByDest)
            console.log(delaybyOrigin)
            console.log(delayByDest)*/
            console.log(flightTimeCountDeparture);
            console.log(flightTimeCountArrival);
            console.log($scope.depdelay+"-"+$scope.arrdelay)
            $scope.c3DestPieChartData = [
                                     ['TPA', countByDest[0]],
                                     ['BWI', countByDest[1]],
                                     ['JAX', countByDest[2]],
                                     ['LAS', countByDest[3]],
                                     ['MCI', countByDest[4]],
                                     ['MCO', countByDest[5]],
                                     
                                     ['MDW', countByDest[6]],
                                     ['PHX', countByDest[7]],
                                     ['FLL', countByDest[8]],
                                     ['PBI', countByDest[9]],
                                     ['RSW', countByDest[10]],
                                     ['HOU', countByDest[11]],
                                     
                                     ['BHM', countByDest[12]],
                                     ['BNA', countByDest[13]],
                                     ['JND', countByDest[14]],
                                     ['ORF', countByDest[15]],
                                     ['ABQ', countByDest[16]],
                                     ['ALM', countByDest[17]],
                                     
                                     ['AMA', countByDest[18]],
                                     ['AUS', countByDest[19]],
                                     ['BDL', countByDest[20]],
                                     ['BOI', countByDest[21]]
                                     
                                 ];
            $scope.c3OriginPieChartData = [
                                           ['LAS', countByOrigin[0]],
                                           ['IND', countByOrigin[1]],
                                           ['IAD', countByOrigin[2]],
                                           ['ISP', countByOrigin[3]],
                                           ['JAN', countByOrigin[4]],
                                           ['JAX', countByOrigin[5]],
                                           
                                           
                                       ];
            $scope.d3BarDelayInDepartures= [
                                    {
                                        "name": "LAS",
                                        "value": delaybyOrigin[0]
                                    },
                                    {
                                        "name": "IND",
                                        "value": delaybyOrigin[1]
                                    },
                                    {
                                        "name": "IAD",
                                        "value": delaybyOrigin[2]
                                    },
                                    {
                                        "name": "ISP",
                                        "value": delaybyOrigin[3]
                                    },
                                    {
                                        "name": "JAN",
                                        "value": delaybyOrigin[4]
                                    },
                                    {
                                        "name": "JAX",
                                        "value": delaybyOrigin[5]
                                    }

                                ];
            
            $scope.flightTimingsArrDept = [
                                     ['Departures', flightTimeCountDeparture[0],flightTimeCountDeparture[1],flightTimeCountDeparture[2],flightTimeCountDeparture[3]],
                                     ['Arrivals', flightTimeCountArrival[0],flightTimeCountArrival[1],flightTimeCountArrival[2],flightTimeCountArrival[3]]
                                 ];
           $scope.flightTimingsArrDeptGroupValue = [
                                        ['Departures', 'Arrivals']
                                    ];
           
           $scope.morningFlights=flightTimeCountDeparture[1]+flightTimeCountDeparture[2];
           $scope.nightFlights=flightTimeCountDeparture[0]+flightTimeCountDeparture[3];
            //6-6
            new RenderOneOFAllChart().c3PieChart($scope.c3OriginPieChartData,'c3originPieChart');
            new RenderOneOFAllChart().c3PieChart($scope.c3DestPieChartData,'c3destPieChart');
            //12
            new RenderOneOFAllChart().d3BarChart($scope.d3BarDelayInDepartures,'d3BarChart')  //Chart Id 3
            //12
            var cat1= ['00:00 - 06:00 ',   ' 06:00 - 12:00 ',   ' 12:00 - 18:00 ',  ' 18:00 - 23:59 ' ];
            new RenderOneOFAllChart().c3BarStackedChart( $scope.flightTimingsArrDept, $scope.flightTimingsArrDeptGroupValue, 'c3StackedBarTimings',cat1) //chart ID -8
       
            });// End of Service Call


            $scope.exportData = function (chartID, chartName) {
            
                RenderExcel(chartID, chartName);
            };

            $scope.pdfRender = function (pdfid, pdfName) {
                var pdfIDData = pdfid + ' ' + 'svg'
              
                RenderPDF(pdfIDData, pdfName)
            }


    });
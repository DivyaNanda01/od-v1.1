/**
 * Created by prateek on 10/2/16.
 */
'use strict';
angular.module('webApp')
    .controller('RoleBasedAccessController', function ($scope,$filter,$rootScope,RoleBasedService,toasty,$cookies,AuthenticationService) {
    	
    	var d1={userName:'user',objectName:'dataAccess'};
    	
    	var tkn = {
        		'x-auth-token':$rootScope.globals.currentUser.authdata        		
        		}
    	console.log($rootScope.globals.currentUser)
    	
    	console.log(tkn);
   
    	
            $scope.onClickTab = function (index) {
                angular.forEach($scope.roleBasedTabs,function(value,key){
                    if(!(index == key))
                        $scope.roleBasedTabs[key].isActive='';
                });
                $scope.roleBasedTabs[index].isActive='active';
                $scope.cancel();

            };
            // Controller for Tab1 - Permissions Html
            $scope.roleSelected=false;
            $scope.showRoleModules = function(){
            	
            	$scope.roleSelected=true;
            }

            
            
       
            
            
            
            //Permissions Mgmt
            $scope.PermissionNames=[];
            $scope.ObjectNames=[];
            $scope.ObjectPermissions=[];
   
            RoleBasedService.getAllPermissionObjects(tkn).then(function(data){
            	
            	/*data.sort(function(a, b){
       			 var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
       			 if (nameA < nameB) //sort string ascending
       			  return -1 
       			 if (nameA > nameB)
       			  return 1
       			 return 0 //default return value (no sorting)
       			});*/
            	angular.forEach(data,function(maindata,mainkey){
            		
            		
            		$scope.PermissionNames=[];	
            		maindata.permission.sort(function(a, b){
            			 var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
            			 if (nameA < nameB) //sort string ascending
            			  return -1 
            			 if (nameA > nameB)
            			  return 1
            			 return 0 //default return value (no sorting)
            			});
            
            		$scope.ObjectPermissions.push({name:maindata.name,permission:maindata.permission}); // Main Data
            		
            		angular.forEach(maindata.permission,function(childData,childKey){
            			$scope.PermissionNames.push({name:childData.name});
            			
            		});
            		
            		$scope.PermissionNames.sort(function(a, b){
           			 var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
        			 if (nameA < nameB) //sort string ascending
        			  return -1 
        			 if (nameA > nameB)
        			  return 1
        			 return 0 //default return value (no sorting)
        			});
            		
            	});
                 
            });
            
       
            
            
            // When Values are Checked
            $scope.checkValue =function(ParentIndex,Index,enabled){
            	//console.log(Object);
            	$scope.ObjectPermissions[ParentIndex].permission[Index].enabled=enabled;
            }
            
            
          //When Permissions are Saved
            $scope.savePermissionData =function(RoleName,PermissionData){
            	console.log(PermissionData);
            	console.log(RoleName)
            	var flag=0; // This flag is used to check if any object has atleast one permission selected
            	/*
            	 * JSON Structure
            	 * [{roleName:'Your Role Name',
            	 *   dashBoardObjects:[{name:dataAccess,permission:[{name:'',enabled:''}]},{...}]
            	 *   }]
            	 */
            	var PermissionArray =[];
            	var EditedPermissions=[];
            	var EditedRolePermissions={};
            	angular.forEach(PermissionData,function(data,key){
            		
            		angular.forEach(data.permission,function(PermissionValues,PermissionKey){
            			console.log(PermissionValues)
            			if(PermissionValues.enabled == true){                				
            				PermissionArray.push(PermissionValues);
            				flag=1;
            				}
            			
            		});
            		
            		if(flag)
            			EditedPermissions.push({name:data.name,type:data.type,description:data.description,permission:PermissionArray});
            		
            		PermissionArray=[];
            		flag=0;
            	});     
            	
            	EditedRolePermissions={roleName:RoleName,dashBoardObjects:EditedPermissions};
            	console.log(EditedRolePermissions);
            	RoleBasedService.saveEditedRolePermissions(EditedRolePermissions,tkn).then(function(data){
            		console.log("yo")
            		console.log(data)
            	});
            }
           
            
            //When Role is changed from Select Box
            $scope.PermissionByRole = function(rolename){
            	
            	RoleBasedService.getObjectPermissionsByRoleName(rolename,tkn).then(function(data){
            		console.log(data)
            		angular.forEach(data,function(maindata,mainkey){
                		maindata.permission.sort(function(a, b){
               			 var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
               			 if (nameA < nameB) //sort string ascending
               			  return -1 
               			 if (nameA > nameB)
               			  return 1
               			 return 0 //default return value (no sorting)
               			});
                	
                
            	});

            		$scope.ObjectPermissions =data;
            });
            	
         }


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //RoleMGMT

            $scope.cancel = function(){
            	$scope.showPermissionPanel=false;
                $scope.showRolePanel=false;                
                $scope.showUserPanel=false;
            }
            $scope.RoleListDetails=[];
            
            RoleBasedService.getAllRoles(tkn).then(function(data){
            	console.log("Incontroller");
            	console.log(data);
            	angular.forEach(data,function(maindata,key){
            		$scope.RoleListDetails.push({
            			name:maindata.name,
            			description:maindata.description,
            			roleId:maindata.id
            		});
            	});
            });
            $scope.showRolePanel=false;
            $scope.editRoleIndex=-1;
            
           
            $scope.addRole = function(){
                $scope.showRolePanel = true;
                $scope.RoleClickType='Add Role';
                $scope.editRoleIndex=-1;
                
                $scope.RoleName='';
                $scope.RoleDescription='';
            }
            $scope.RoleMainID=-1;
            
            $scope.editRole = function(index,data){
                $scope.showRolePanel = true;
                $scope.RoleClickType='Edit Role';
                $scope.editRoleIndex=index; // this is id in which items are arranged
                
                //Data to be placed Here
                $scope.RoleName=data.name;
                $scope.RoleDescription=data.description;
                $scope.RoleMainID=data.roleId; // This is role id from database
             }

            
            $scope.removeRole = function(index,data){
            	var flag =0;
            
            	angular.forEach($scope.UserListDetails,function(maindata,key){
            		if(maindata.roleName == data.name)
            			flag=1;            			
            	});
            
            	if (flag == 0){
            			// Delete Role Related Permissions
            	/*	var RemoveRolePermissionData= {roleName:data.name, dashBoardObjects:[] };
            		console.log(RemoveRolePermissionData);
            		
            		RoleBasedService.saveEditedRolePermissions(RemoveRolePermissionData,tkn).then(function(data){
            			console.log("Permissions Deleted");
            			console.log(data);
            		});
            		console.log("okay");
            	*/	
            		
            		//Delete Role
		            	RoleBasedService.deleteSelectedRole(data.name,tkn).then(function(data1){
		            		console.log("Deleting Role")
		            		console.log(data1)
		            		$scope.RoleListDetails.splice(index,1);
		            	});
            		}
            	
            		else{
            			 toasty.pop.error({
            		         title: 'Error',
            		         msg: "Cannot Delete Role because it is assigned to the some Users " ,
            		         	
            		         timeout: 4500,
            		         showClose: true,
            		         clickToClose: true
            		     });
            		}
            	/*}    */   
                
            }

            
            $scope.saveRole = function(RoleName,RoleDescription){
                if($scope.editRoleIndex == -1) { // AddRole
                	var AddRoleData ={ name:RoleName,description:RoleDescription};
                	RoleBasedService.saveNewRole(AddRoleData,tkn).then(function(data){
                		$scope.RoleListDetails.push({name:data.name,
                			description:data.description,
                			roleId:data.id});
                	});
                    /*$scope.userGroupListDetails.push({name : userGroupName,description:userGroupDescription, roleSelect:roleSelect});*/
                }


                else{ //Edit User Group
                	
                	var EditRoleData ={ id:$scope.RoleMainID,name:RoleName,description:RoleDescription};
                	console.log(EditRoleData)
                	RoleBasedService.saveEditedRole(EditRoleData,tkn).then(function(data){
                		
                		$scope.RoleListDetails[$scope.editRoleIndex].name = data.name;
                        $scope.RoleListDetails[$scope.editRoleIndex].description =data.description;
                        $scope.RoleListDetails[$scope.editRoleIndex].id= data.id;
                       
                	})
                	
                }
                $scope.showRolePanel = false;

            }


















            //User Mgmt

            $scope.UserListDetails=[];
            $scope.showUserPanel=false;
            $scope.editUserIndex=-1;
            
            RoleBasedService.getAllUsers(tkn).then(function(data){
            	console.log("Alluser Data");
            	console.log(data)
        		
        		angular.forEach(data,function(maindata,mainkey){
        			$scope.UserListDetails.push({
        				fName:maindata.firstName,
        				lName:maindata.lastName,
        				userName:maindata.username,
        				emailId:maindata.email,
        				roleName:maindata.dashBoardRole.name,        				
        				Password:maindata.password,
        				enabled:(maindata.enabled==true)?1:0,
        				createdBy:maindata.createdBy,
        				createdDate:maindata.createdDate,
        				
        			});
        		});
        	});


            $scope.addUser = function(){
                $scope.showUserPanel = true;
                $scope.UserMgmtClickType='Add User';
                $scope.editUserIndex=-1;
                $scope.userNameInputDisabled=false;
                
                $scope.FirstName='';
                $scope.LastName='';
                $scope.UserName='';
                $scope.EmailAddress='';
                $scope.Password='';
                $scope.groupSelect='';
                $scope.enabled='';
                $scope.createdDate='';
                $scope.createdBy='';
            }

            $scope.editUser = function(index,data){
            	console.log(data)
                $scope.showUserPanel = true;
                $scope.editUserIndex=index;
                $scope.userNameInputDisabled=true;
                $scope.UserMgmtClickType='Edit User';
                
                $scope.FirstName=data.fName;
                $scope.LastName=data.lName;
                $scope.UserName=data.userName,
                $scope.EmailAddress=data.emailId,
                $scope.Password=data.Password;
                $scope.groupSelect=data.roleName;
                $scope.enabled=data.enabled;
                $scope.createdDate=data.createdDate;
                $scope.createdBy=data.createdBy;
            }
            
           

            $scope.removeUser = function(index,data){
            	if($rootScope.globals.currentUser.username == data.userName)
            		alert("You are logged in from this Account. Cannot delete this user");
            	else{
	                //var deleteUser= {UserName:data.userName};
	            	RoleBasedService.deleteSelectedUser(data.userName,tkn).then(function(data1){
	            		$scope.UserListDetails.splice(index,1);
	            	});
            	}        
            }
            $scope.enabled=1;
        	
        	$scope.saveUserData = function(FirstName,LastName,UserName,EmailAddress,Password,enabled,groupSelect){
        		console.log(groupSelect)
           		if($scope.editUserIndex == -1)
            		{
            			//Add User - For add user edituserndex=-1
                    	var UserAddedData={
                    			createdBy : $rootScope.globals.currentUser.username,   
                    			createdDate: null, //added from backend
                    			email:EmailAddress,
                    			enabled:(enabled==1)?true:false,
                    			firstName:FirstName,
                    			lastModifiedBy:$rootScope.globals.currentUser.username,
                    			lastModifiedDate:null, //added from backend
                    			lastName:LastName,
                    			password:Base64.encode(Password),
                    			username:UserName,
                    			dashBoardRole:{
                    				name :groupSelect,
                    				description:null
                    			}
                    	}
                		RoleBasedService.saveNewUser(UserAddedData,tkn).then(function(data){
                			console.log(data)
                			 $scope.UserListDetails.push({   				
                      			fName:data.firstName,
                  				lName:data.lastName,
                  				userName:data.username,
                  				emailId:data.email,
                  				roleName:data.dashBoardRole.name,                  				
                  				Password:data.password,
                  				enabled:(data.enabled == true)?1:0,                  				
                  				createdBy:data.createdBy,
                  				createdDate:data.createdDate,
                      		 });;
                		});
                
                    }
            		else{
                
               		 var UserEditedData={
               				 	createdBy : $scope.createdBy,   
                    			createdDate: $scope.createdDate,
                    			email:EmailAddress,
                    			enabled:(enabled==1)?true:false,
                    			firstName:FirstName,
                    			lastModifiedBy:$rootScope.globals.currentUser.username,
                    			lastModifiedDate:null,
                    			lastName:LastName,
                    			password:Base64.encode(Password),
                    			username:UserName,
                    			dashBoardRole:{
                    				name :groupSelect,
                    				description:null
                    			}
                    	}
                		RoleBasedService.saveEditedUser(UserEditedData,tkn).then(function(data,status){
                			
                			//Updating Our Local List after Edit
                            $scope.UserListDetails[$scope.editUserIndex].fName=FirstName;
                            $scope.UserListDetails[$scope.editUserIndex].lName=LastName;
                            $scope.UserListDetails[$scope.editUserIndex].userName=UserName;
                            $scope.UserListDetails[$scope.editUserIndex].emailId=EmailAddress;
                            $scope.UserListDetails[$scope.editUserIndex].roleName=groupSelect;
                            $scope.UserListDetails[$scope.editUserIndex].Password=Password,
                            $scope.UserListDetails[$scope.editUserIndex].enabled=enabled;
              				
                		});
            			
            			
            		}
                	 $scope.showUserPanel = false;
           	}

		var Base64 = {

			keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

			encode: function (input) {
				var output = "";
				var chr1, chr2, chr3 = "";
				var enc1, enc2, enc3, enc4 = "";
				var i = 0;

				do {
					chr1 = input.charCodeAt(i++);
					chr2 = input.charCodeAt(i++);
					chr3 = input.charCodeAt(i++);

					enc1 = chr1 >> 2;
					enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
					enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
					enc4 = chr3 & 63;

					if (isNaN(chr2)) {
						enc3 = enc4 = 64;
					} else if (isNaN(chr3)) {
						enc4 = 64;
					}

					output = output +
						this.keyStr.charAt(enc1) +
						this.keyStr.charAt(enc2) +
						this.keyStr.charAt(enc3) +
						this.keyStr.charAt(enc4);
					chr1 = chr2 = chr3 = "";
					enc1 = enc2 = enc3 = enc4 = "";
				} while (i < input.length);

				return output;
			},

			decode: function (input) {
				var output = "";
				var chr1, chr2, chr3 = "";
				var enc1, enc2, enc3, enc4 = "";
				var i = 0;

				// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
				var base64test = /[^A-Za-z0-9\+\/\=]/g;
				if (base64test.exec(input)) {
					window.alert("There were invalid base64 characters in the input text.\n" +
						"Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
						"Expect errors in decoding.");
				}
				input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

				do {
					enc1 = this.keyStr.indexOf(input.charAt(i++));
					enc2 = this.keyStr.indexOf(input.charAt(i++));
					enc3 = this.keyStr.indexOf(input.charAt(i++));
					enc4 = this.keyStr.indexOf(input.charAt(i++));

					chr1 = (enc1 << 2) | (enc2 >> 4);
					chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
					chr3 = ((enc3 & 3) << 6) | enc4;

					output = output + String.fromCharCode(chr1);

					if (enc3 != 64) {
						output = output + String.fromCharCode(chr2);
					}
					if (enc4 != 64) {
						output = output + String.fromCharCode(chr3);
					}

					chr1 = chr2 = chr3 = "";
					enc1 = enc2 = enc3 = enc4 = "";

				} while (i < input.length);

				return output;
			}
		};
    });

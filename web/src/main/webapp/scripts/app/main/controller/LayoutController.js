'use strict';
angular.module('webApp')
    .controller('LayoutController', function ($scope, DataSource,$filter,$rootScope) {
       
        
          

     // Data for Chart Id 0 - c3 Bar Chart Data
		$scope.c3BarChartData = [
		                         ['x', '2013-10-31', '2013-12-31', '2014-01-31', '2014-02-28'],
		                         ['sample', 30, 100, 400, 150],
                         ];


        //Data for Chart Id - 1 - Pie Chart c3
        $scope.c3PieChartData = [
                          ['data1', 30, 200, 100, 400, 150, 250],
                          ['data2', 130, 100, 140, 200, 150, 50],
                          ['data3', 10, 140, 240, 100, 50, 50]
                          
                          
                      ];
        
      //Data for Chart Id - 2 - Donut Chart c3
        $scope.c3DonutChartData = [
                          ['data1', 30, 200, 100, 400, 150, 250],
                          ['data2', 130, 100, 140, 200, 150, 50]
                      ];
        
        //Data for Chart Id -3 - d3 Bar Chart
        $scope.d3BarChartData= [
                       {
                           "name": "A",
                           "value": 10
                       },
                       {
                           "name": "B",
                           "value": 20
                       },
                       {
                           "name": "C",
                           "value": 30
                       },
                       {
                           "name": "D",
                           "value": 40
                       }

                   ];
        
        	//Data for Chart Id - 4 - c3 Gauge Chart
        		$scope.c3GaugeChartData = [
                         ['data', 25.0]
                     ];
        		
            //Data for Chart ID - 8 - c3 Stacked Bar Data
                $scope.stackedDataBar = [
                               ['data1', 30, 200, 100, 400, 150, 250],
                               ['data2', 130, 100, 140, 200, 150, 50]
                           ];
                $scope.groupvalue = [
                                  ['data1', 'data2']
                              ]

        	
          //div row 4-4-4
        	new RenderOneOFAllChart().c3BarChart($scope.c3BarChartData, 'c3BarChart') //Chart Id 0
            new RenderOneOFAllChart().c3PieChart($scope.c3PieChartData,'c3pieChart'); //chart id 1
            new RenderOneOFAllChart().c3DonutChart($scope.c3DonutChartData, 'c3DonutChart')  //Chart Id 2
            
          //div row 8-4
            new RenderOneOFAllChart().d3BarChart($scope.d3BarChartData,'d3BarChart')  //Chart Id 3
            new RenderOneOFAllChart().c3GaugeChart($scope.c3GaugeChartData, 'c3GaugeChart') // Chart Id 4
            	
          //div row 4-4(repeat)-4
             new RenderOneOFAllChart().d3RadialProgress(160,'d3RadialProgress','Radial');  //Chart Id 5
             new RenderOneOFAllChart().d3RadialProgress(240,'d3RadialProgress2','Radial'); //Chart Id 6
             new RenderOneOFAllChart().d3LiquidFillGauge(50,'d3LiquidFillGauge')   //Chart Id 7
           
            //div row 12
             	new RenderOneOFAllChart().c3BarStackedChart( $scope.stackedDataBar, $scope.groupvalue, 'c3stackedbar') //chart ID -8
            
             	
            
             $scope.exportData = function (chartID,chartName) {
            	 console.log(chartID)
                 RenderExcel(chartID, chartName);
             };

             $scope.pdfRender = function (pdfid, pdfName) {
                 var pdfIDData = pdfid + ' ' + 'svg'
                 
                 RenderPDF(pdfIDData, pdfName)
             }
             
             

           
    });

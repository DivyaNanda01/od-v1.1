'use strict';

angular.module('webApp')
    .controller('NavbarController', function ($scope, $location, $rootScope,$state,$http,  $q, ENV,NavBarService,AuthenticationService) {



        //get User Theme
    	var tkn =$rootScope.globals.currentUser.authdata        		
        		
        $rootScope.userThemeColor="blueTheme";

        $scope.logout = function () {

            AuthenticationService.ClearCredentials();
            NavBarService.clearToken(tkn).then(function(data){

            });
            
            //$state.go('login');
        };
        
       
        

        $scope.hideshow=function(parent){
           this.open=!this.open
        }

    });

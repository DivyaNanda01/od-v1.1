'use strict';

angular.module('webApp')
    .service('NavBarService', function ($rootScope,$http,  $q ) {


        this.clearToken = function(tkn){

        	var defer = $q.defer();

        	$http({
        			 method:'DELETE',
        			 url:'logout',
        			 headers: {"Content-Type": "application/json" ,"x-auth-token":tkn}
        	})
        	.success(function(data,status,header,config){

        		defer.resolve(data);
        	})
        	.error(function(data,status){

        		defer.reject(data,status)
        	});
        	return defer.promise;
        }
        

    });

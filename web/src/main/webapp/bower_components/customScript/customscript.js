function RenderOneOFAllChart(){

    this.d3RadialProgress = function(data,idRender,labelName) {
            var rp3 = radialProgress(document.getElementById(idRender))
                .label(labelName)
                .diameter(180)
                .minValue(0)
                .maxValue(400)
                .value(data)
                .render();

        }
    this.d3LiquidFillGauge =function(data,idRender){
        d3.select('#'+idRender).append("svg:svg")
            .attr('id','liquidid').attr('width','200').attr('height','314');
        var chartLiguid = liquidFillGaugeDefaultSettings();
        chartLiguid.circleThickness = 0.15;
        chartLiguid.textVertPosition = 0.8;
        chartLiguid.waveAnimateTime = 1000;
        chartLiguid.waveHeight = 0.05;
        chartLiguid.waveAnimate = true;
        chartLiguid.waveRise = false;
        chartLiguid.waveHeightScaling = false;
        chartLiguid.waveOffset = 0.25;
        chartLiguid.textSize = 0.75;
        chartLiguid.waveCount = 3;
        var chartLiguidRender = loadLiquidFillGauge('liquidid', data, chartLiguid);
    }
    this.d3BarChart =function(data,idRender){
        var widthRender = document.getElementById(idRender).offsetWidth;
        var margin = {top: 40, right: 20, bottom: 30, left: 40},
            width = widthRender - margin.left - margin.right,
            height = 350 - margin.top - margin.bottom;

        var formatPercent = d3.format(".0%");

        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function(d) {
                return '<strong>'+d.name+': </strong> <span>' + d.value + '</span>';
            })

        var svg = d3.select('#'+idRender).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.call(tip);


            x.domain(data.map(function(d) { return d.name; }));
            y.domain([0, d3.max(data, function(d) { return d.value; })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end");

            svg.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function(d) { return x(d.name); })
                .attr("width", x.rangeBand())
                .attr("y", function(d) { return y(d.value); })
                .attr("height", function(d) { return height - y(d.value); })
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);




        function type(d) {
            d.value = +d.value;
            return d;
        }

    }
    this.c3BarChartXValue =function(data,idRender,chartName){

        var chartone1 = c3.generate({
            bindto: '#'+idRender,
            data: {
                x : data[0][0],
                columns: data,
                type: 'bar'
            },
            axis: {
                x: {
                    type: 'category' // this needed to load string x value
                }
            }
        });
    }
    this.c3BarChart =function(data,idRender){
        var chartone = c3.generate({
            bindto: '#'+idRender,
            data: {
                columns:data,
                type: 'bar'
            }
        });
    }
    this.c3BarStackedChart =function(data,datagroup,idRender,cat){
    	console.log("yo"+cat);
        var chartone = c3.generate({
            bindto: '#'+idRender,
            data: {
                columns: data,
                type: 'bar',
                groups: datagroup
                
            },
            axis: {
            	  x: {
            	    show: true,
            	    type: 'category',
                    categories:cat
            	  }
            	},
            grid: {
                y: {
                    lines: [{value:0}]
                },
                x:{
                	lines:[{values:'sss'}]
                }
            }
        });
    }
    this.c3PieChart =function(data,idRender){
        var chartone = c3.generate({
            bindto: '#'+idRender,
            data: {
                columns:data,
                type: 'pie'
            }
        });
    }
    this.c3DonutChart =function(data,idRender){
        var chartone = c3.generate({
            bindto: '#'+idRender,
            data: {
                columns:data,
                type: 'donut'
            }
        });
    }
    this.c3GaugeChart =function(data,idRender){
        var chart = c3.generate({
            bindto: '#'+idRender,
            data: {
                columns: data,
                type: 'gauge',
            },
            min: 0,
            max: 100,
            size: {
                height: 320
            },
            padding: {
                bottom: 80
            }
        });
    }


}

function RenderExcel(data,ExcelName){
    alasql('SELECT * INTO XLSX("'+ExcelName+'.xlsx",{headers:true}) FROM ?',[data]);
}

function RenderPDF(pdfid,pdfName){
    document.querySelector(pdfid).style.background='white';
    svg_to_pdf(document.querySelector(pdfid), function (pdf) {
        download_pdf(''+pdfName+'.pdf', pdf.output('dataurlstring'));
    });
}


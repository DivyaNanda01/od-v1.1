/**
 * 
 */
package com.datametica.web.model.security;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahendra
 *
 */
@JsonAutoDetect
public class AuthenticationResponse implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8539815327384650399L;
	@JsonProperty
	private String userName;
	@JsonProperty
	private Set<DashBoardObject> dashBoardObjectSet;
	@JsonProperty
	private String createToken;

	public AuthenticationResponse() {
	}
	public AuthenticationResponse(String username,Set<DashBoardObject> dashBoardObjectSet, String createToken) {
		this.userName = username;
		this.createToken = createToken;
		this.dashBoardObjectSet = dashBoardObjectSet;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreateToken() {
		return createToken;
	}

	public void setCreateToken(String createToken) {
		this.createToken = createToken;
	}

	@Override
	public String toString() {
		return "{AuthenticatinoResponse : userName: "+userName+" createToken: "+createToken+" "
				+ "dashBoardObjectSet : "+dashBoardObjectSet+"}";
		
	}
	public Set<DashBoardObject> getDashBoardObjectSet() {
		return dashBoardObjectSet;
	}
	public void setDashBoardObjectSet(Set<DashBoardObject> dashBoardObjectSet) {
		this.dashBoardObjectSet = dashBoardObjectSet;
	}

}
package com.datametica.web.model.security;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DashBoardUser implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6248461363325041484L;

	private String username;
	private String password;
	private boolean enabled;
	private String firstName;
	private String lastName;
	private String email;
	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	@JsonIgnore
	private Set<GrantedAuthority> authorities;
	
	private Set<DashBoardObject> dashBoardobjectSet;
	
	private DashBoardRole dashBoardRole;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return enabled;
	}

	@Override
	public boolean isAccountNonLocked() {
		return enabled;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return enabled;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public DashBoardRole getDashBoardRole() {
		return dashBoardRole;
	}

	public void setDashBoardRole(DashBoardRole dashBoardRole) {
		this.dashBoardRole = dashBoardRole;
	}

	@Override
	public String toString() {
		return "{DashboardUser : userName : " + username + " enabled : " + enabled + " firstName : " + firstName
				+ " lasName : " + lastName + "}";
	}

	public Set<DashBoardObject> getDashBoardobjectSet() {
		return dashBoardobjectSet;
	}

	public void setDashBoardobjectSet(Set<DashBoardObject> dashBoardobjectSet) {
		this.dashBoardobjectSet = dashBoardobjectSet;
	}

}
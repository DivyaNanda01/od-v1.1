/**
 * 
 */
package com.datametica.web.model.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author mahendra
 *
 */
public class DashBoardRole implements GrantedAuthority{



	/**
	 * 
	 */
	private static final long serialVersionUID = 5893156842043427580L;
	private Integer id;
	private String name;
	private String description;
	public DashBoardRole() {
	}
	public DashBoardRole(Integer id, String name,String description){
		this.id=id;
		this.name=name;
		this.description=description;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}

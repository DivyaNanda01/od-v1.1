/**
 * 
 */
package com.datametica.web.model.security;

import java.io.Serializable;
import java.util.Set;

/**
 * @author mahendra
 *
 */
public class DashBoardObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3843008629202449531L;
	private String name;
	private String description;
	private String type;
	Set<DashBoardPermission> permission;
	public DashBoardObject() {
	}
	public DashBoardObject(String name) {
		this.name=name;
	}
	
	public DashBoardObject(String name, String description, String type) {
		this.name=name;
		this.description=description;
		this.type=type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "{DashBoardObject : name :"+name+" permission : "+permission+" type : "+type+"}";
	}
	public Set<DashBoardPermission> getPermission() {
		return permission;
	}
	public void setPermission(Set<DashBoardPermission> permission) {
		this.permission = permission;
	}

}

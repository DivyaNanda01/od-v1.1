/**
 * 
 */
package com.datametica.web.model.security;

import java.io.Serializable;

/**
 * @author mahendra
 *
 */
public class DashBoardPermission implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1994877058614227221L;
	private String name;
	private String description;
	private boolean enabled;

	public DashBoardPermission() {
	}

	public DashBoardPermission(String name, String description, boolean enabled) {
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "{DashBoardPermission : name : "+name+" description : "+description+" enabled : "+enabled+"}";
	}
}

package com.datametica.web.model.security;

import java.io.Serializable;
import java.util.Set;

public class DashBoardRolePermissions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5074391107305414046L;
	
	private String roleName;
	
	private Set<DashBoardObject> dashBoardObjects;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<DashBoardObject> getDashBoardObjects() {
		return dashBoardObjects;
	}

	public void setDashBoardObjects(Set<DashBoardObject> dashBoardObjects) {
		this.dashBoardObjects = dashBoardObjects;
	}
	@Override
	public String toString() {
		return "{ DashBoardRolePermissions : roleName : "+roleName+" dashBoardObjects : "+dashBoardObjects+"}";
	}
}

package com.datametica.web.api;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;

import javax.sql.DataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.impl.CompositeRegistry;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datametica.businessconfigurator.service.IDataConfigService;
import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IQuery;
import com.datametica.queryexecuter.service.impl.QueryManager;
import com.datametica.web.AbstractRouteBuilder;

@Component
public class GraphDataRoute extends AbstractRouteBuilder {

	@Autowired
	IDataConfigService dataConfigService;

	@Autowired
	QueryManager queryManager;

	@Override
	public void configure() throws Exception {
		super.configure();
		// servlet is configured in Application.java
		restConfiguration().component("servlet").bindingMode(RestBindingMode.json);

		rest("/getData").get().to("direct:getData");

		from("direct:getData").process(exchange -> {
			CamelContext camelContext = exchange.getContext();
			Message message = exchange.getIn();

			CompositeRegistry cRegistry = new CompositeRegistry();
			SimpleRegistry sRegistry = new SimpleRegistry();

			String queryName = message.getHeader("queryname", String.class);
			String filterConditions = message.getHeader("filterconditions", String.class);
			assertNotNull("'queryName' not provided ", queryName);

			IQuery query = dataConfigService.getQueryByName(queryName);
			QueryConfig queryConfig = new QueryConfig(query, filterConditions, null);

			String queryString = queryManager.getQuery(queryConfig);
			DataSource datasource = queryManager.getDatasource(queryConfig);
			String dataSourceName = query.getDataSource().getName();

			message.setBody(queryString);
			message.setHeader("datasource", dataSourceName);

			cRegistry.addRegistry(camelContext.getRegistry());
			sRegistry.put(dataSourceName, datasource);
			cRegistry.addRegistry(sRegistry);
			((DefaultCamelContext) camelContext).setRegistry(cRegistry);

		}).dynamicRouter(method(DynamicRouter.class, "execute"));
	}
}

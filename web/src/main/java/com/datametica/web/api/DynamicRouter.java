package com.datametica.web.api;

import org.apache.camel.Exchange;
import org.apache.camel.Header;

public class DynamicRouter {
	public String execute(@Header(Exchange.SLIP_ENDPOINT) Object previous, @Header("datasource") String datasource)
			throws Exception {

		String component = "jdbc:";
		String options = "?resetAutoCommit=false";

		if (previous == null) {
			return component + datasource + options;
		} else {
			return null;
		}

	}
}
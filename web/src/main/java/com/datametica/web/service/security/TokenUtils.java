/**
 * 
 */
package com.datametica.web.service.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

import com.datametica.web.model.security.TokenInfo;

/**
 * @author mahendra
 *
 */
@Component
public class TokenUtils {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static final Object MAGIC_KEY = "obfuscate";

	private final Map<String,UserDetails> validUsers = new HashMap<>();
	private final Map<UserDetails,TokenInfo> tokens = new HashMap<>();
	public String createToken(UserDetails userDetails) {
		log.info("Creating token for user : {} ",userDetails.getUsername());
		long expires = System.currentTimeMillis() + 1000L * 60 * 60;
		String tokenCreated =userDetails.getUsername() + ":" + expires + ":" + computeSignature(userDetails, expires);
		log.info("Created token is : {} ",tokenCreated);
		validUsers.put(tokenCreated, userDetails);
		tokens.put(userDetails, new TokenInfo(tokenCreated, userDetails));
		return tokenCreated;

	}

	public String getUserNameFromToken(String authToken) {
		if (null == authToken) {
			return null;
		}
		String[] parts = authToken.split(":");
		return parts[0];
	}

	public boolean validateToken(String authToken, UserDetails userDetails) {
		String[] parts = authToken.split(":");
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];
		String signatureToMatch = computeSignature(userDetails, expires);
		return expires >= System.currentTimeMillis() && signature.equals(signatureToMatch);
	}
	public void removeToken(UserDetails userDetails){
		//validUsers.remove(token);
		TokenInfo tokenInfo = tokens.remove(userDetails);
		String token = tokenInfo.getToken();
		validUsers.remove(token);
		log.info("Successfully removed token : {} and user : {} from cache.",token,userDetails);
	}
	private String computeSignature(UserDetails userDetails, long expires) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername()).append(":");
		signatureBuilder.append(expires).append(":");
		signatureBuilder.append(userDetails.getPassword()).append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY);

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	/** returns user based on token 
	 * @param token
	 * @return
	 */
	public UserDetails getUser(String token) {
		return validUsers.get(token);
	}
	

}

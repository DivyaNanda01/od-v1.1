package com.datametica.web.service.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.user.BusinessPermission;
import com.datametica.businessconfigurator.model.user.BusinessRoles;
import com.datametica.businessconfigurator.model.user.BusinessUser;
import com.datametica.businessconfigurator.service.user.IUserService;
import com.datametica.web.model.security.DashBoardObject;
import com.datametica.web.model.security.DashBoardPermission;
import com.datametica.web.model.security.DashBoardUser;

/**
 * @author Mahendra
 *
 */
@Service
public class DashBoardUserService implements UserDetailsService {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	public static final String ROLE_ADMIN = "ADMIN";
	public static final String ROLE_USER = "USER";

	private final IUserService userService;

	@Autowired
	public DashBoardUserService(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		log.info("Loading user with userName : {} from database ", userName);
		BusinessUser user = userService.getUserByUserName(userName);
		if (user.getUsername() == null) {
			log.error("Username : {} not found in dashboard database.", userName);
			throw new UsernameNotFoundException(
					String.format("userName : %s not found in dashboard database", userName));
		}
		return populateDashBoardUser(user);
	}

	/**
	 * @param user
	 * @return
	 */
	private DashBoardUser populateDashBoardUser(BusinessUser businessUser) {
		DashBoardUser dashBoardUser = new DashBoardUser();
		BeanUtils.copyProperties(businessUser, dashBoardUser);
		BusinessRoles businessRole = businessUser.getBusinessRole();
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority(businessRole.getName()));
		dashBoardUser.setAuthorities(authorities);
		Set<BusinessPermission> businessPermission = businessRole.getPermissions();
		if (businessPermission != null && !businessPermission.isEmpty()) {
			Map<String, DashBoardObject> dashBoardMap = new HashMap<>();
			Set<DashBoardObject> dashBoardObjectSet = new HashSet<>();
			for (BusinessPermission permission : businessPermission) {
				String objectName = permission.getObject().getName();
				DashBoardPermission dashBoardPermission = new DashBoardPermission(permission.getName(),permission.getDescription(), true);
				if (!dashBoardMap.containsKey(objectName)) {
					Set<DashBoardPermission> objectPermissions = new HashSet<>();
					objectPermissions.add(dashBoardPermission);
					DashBoardObject object = new DashBoardObject(objectName,permission.getObject().getDescription(), permission.getObject().getType());
					object.setPermission(objectPermissions);
					dashBoardObjectSet.add(object);
					dashBoardMap.put(objectName,object);
					continue;
				}
				DashBoardObject dashBoardObject = dashBoardMap.get(objectName);
				Set<DashBoardPermission> objectPermissions=dashBoardObject.getPermission();
				objectPermissions.add(dashBoardPermission);
			}
			dashBoardUser.setDashBoardobjectSet(dashBoardObjectSet);
		}
		return dashBoardUser;
	}

}
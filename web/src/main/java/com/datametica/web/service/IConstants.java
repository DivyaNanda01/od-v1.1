package com.datametica.web.service;

public class IConstants {
	public static final String xAuthTokenHeaderName = "x-auth-token";
	public static final String CAMEL_URL_MAPPING = "/api/rest/*";
	public static final String CAMEL_SERVLET_NAME = "CamelServlet";
}

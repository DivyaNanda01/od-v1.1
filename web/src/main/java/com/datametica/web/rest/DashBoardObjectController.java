/**
 * 
 */
package com.datametica.web.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datametica.businessconfigurator.model.user.BusinessObject;
import com.datametica.businessconfigurator.model.user.BusinessPermission;
import com.datametica.businessconfigurator.model.user.BusinessRoles;
import com.datametica.businessconfigurator.service.user.IBusinessObjectService;
import com.datametica.businessconfigurator.service.user.IBusinessPermission;
import com.datametica.businessconfigurator.service.user.IRoleService;
import com.datametica.web.model.security.DashBoardObject;
import com.datametica.web.model.security.DashBoardPermission;
import com.datametica.web.model.security.DashBoardRolePermissions;
import com.sun.xml.internal.rngom.digested.DSchemaBuilderImpl;

/**
 * @author mahendra
 *
 */
@RestController
public class DashBoardObjectController {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IBusinessObjectService businessObjectService;

	@Autowired
	private IBusinessPermission businessPermissionService;

	@Autowired
	private IRoleService roleService;

	@RequestMapping(value = "/api/admin/rest/objects", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllObjects() {
		log.info("Rest request to get all permissions in system");
		Set<String> businessObjects = businessObjectService.getBusinessObjectNames();
		Set<String> permissions = businessPermissionService.getAllPermissions();
		Set<DashBoardObject> dashBoardObjects = new HashSet<DashBoardObject>();
		for (String businessObject : businessObjects) {
			DashBoardObject dashBoardObject = new DashBoardObject(businessObject);
			Set<DashBoardPermission> dashBoardPermissionSet = new HashSet<>();
			for (String businessPermission : permissions) {
				DashBoardPermission permission = new DashBoardPermission(businessPermission, null, false);
				dashBoardPermissionSet.add(permission);
			}
			dashBoardObject.setPermission(dashBoardPermissionSet);
			dashBoardObjects.add(dashBoardObject);
		}
		log.info("DashBoard objects are  : {} ", dashBoardObjects);
		return ResponseEntity.ok(dashBoardObjects);
	}

	@RequestMapping(value = "/api/admin/rest/permissions", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editRolePermissions(@RequestBody DashBoardRolePermissions dashBoardRolePermissions) {
		log.info("Rest request to edit dashboard permissions : {} ", dashBoardRolePermissions);
		String roleName = dashBoardRolePermissions.getRoleName();
		try {
			Set<DashBoardObject> dashBoardObjects = dashBoardRolePermissions.getDashBoardObjects();
			BusinessRoles businessRole = roleService.getRole(roleName);
			Set<BusinessPermission> rolePermissions = new HashSet<>();
			for (DashBoardObject dashBoardObject : dashBoardObjects) {
				Set<DashBoardPermission> dashBoardPermissionSet = dashBoardObject.getPermission();
				Set<BusinessPermission> objectPermissions = new HashSet<>();
				BusinessObject businessObject = businessObjectService.getBusinessObject(dashBoardObject.getName());
				for (DashBoardPermission dashBoardPermission : dashBoardPermissionSet) {
					BusinessPermission businessPermission = businessPermissionService
							.getBusinessPermissionBynameAndobject(dashBoardPermission.getName(), businessObject);
					if(businessPermission==null){
						businessPermission = new BusinessPermission(dashBoardPermission.getName(), dashBoardPermission.getDescription());
					}
					businessPermission.setObject(businessObject);
					objectPermissions.add(businessPermission);
				}
				businessObject.setPermission(objectPermissions);
				rolePermissions.addAll(objectPermissions);
			}
			businessRole.setPermissions(rolePermissions);
			roleService.editRole(businessRole);
			log.info("Successfully edited role : {} and corresponding permissions", roleName);
		} catch (Exception e) {
			log.error(String.format("Exception occured while editing permissions of Role : %s", roleName), e);
			return ResponseEntity.ok(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok(HttpStatus.OK);
	}

	@RequestMapping(value = "/api/admin/rest/permissions/{roleName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRolePermissions(@PathVariable String roleName) {
		log.info("Rest request to get all  permissions for role : {} ", roleName);
		BusinessRoles businessRole = roleService.getRole(roleName);
		Set<BusinessPermission> businessPermissions = businessRole.getPermissions();
		Map<String, DashBoardObject> dashBoardMap = populateDashBoardObjectMap(businessPermissions);

		// below for loop will add the objects to dashBoardMap if these objects
		// are not present in dashboard map
		// objects will not be present in map only if role is not assigned any
		// permission on it
		List<BusinessObject> businessObjects = businessObjectService.getBusinessObjects();
		for (BusinessObject businessObject : businessObjects) {
			String businessObjectName = businessObject.getName();
			if (!dashBoardMap.containsKey(businessObjectName)) {
				dashBoardMap.put(businessObjectName, new DashBoardObject(businessObjectName,
						businessObject.getDescription(), businessObject.getType()));
				continue;
			}
		}

		Set<DashBoardObject> finalDashBoardObjectSet = addDisabledPermissionsToMap(dashBoardMap);
		return ResponseEntity.ok(finalDashBoardObjectSet);
	}

	/**
	 * This method populates dashBoardObject with those permissions which are
	 * not assigned to dashboard role on object and permissions value will be
	 * false
	 * 
	 * @param dashBoardMap
	 * @return
	 */
	private Set<DashBoardObject> addDisabledPermissionsToMap(Map<String, DashBoardObject> dashBoardMap) {
		Set<DashBoardObject> finalDashBoardObjectSet = new HashSet<>();
		Set<String> allPermissionsInSystem = businessPermissionService.getAllPermissions();
		for (Map.Entry<String, DashBoardObject> dashBoardEntry : dashBoardMap.entrySet()) {
			DashBoardObject dashBoardObject = dashBoardEntry.getValue();
			Set<String> existingPermissions = new HashSet<>();
			Set<DashBoardPermission> dashBoardPermissionSet = new HashSet<>();
			if (dashBoardObject.getPermission() != null && !dashBoardObject.getPermission().isEmpty()) {
				dashBoardPermissionSet.addAll(dashBoardObject.getPermission());
				for (DashBoardPermission dashBoardPermission : dashBoardPermissionSet) {
					existingPermissions.add(new String(dashBoardPermission.getName()));
				}
			}
			for (String notPresentPermission : allPermissionsInSystem) {
				if (!existingPermissions.contains(notPresentPermission)) {
					dashBoardPermissionSet.add(new DashBoardPermission(notPresentPermission, null, false));
				}
			}
			dashBoardObject.setPermission(dashBoardPermissionSet);
			finalDashBoardObjectSet.add(dashBoardObject);
		}
		return finalDashBoardObjectSet;
	}

	/**
	 * This method populates dashboard object map with existing true values
	 * 
	 * @param businessPermissions
	 * @return
	 */
	private Map<String, DashBoardObject> populateDashBoardObjectMap(Set<BusinessPermission> businessPermissions) {
		Map<String, DashBoardObject> dashBoardMap = new HashMap<>();
		for (BusinessPermission permission : businessPermissions) {
			BusinessObject object = permission.getObject();
			String objectName = object.getName();
			DashBoardPermission dashBoardPermission = new DashBoardPermission(permission.getName(),
					permission.getDescription(), true);
			if (!dashBoardMap.containsKey(objectName)) {
				DashBoardObject dashBoardObject = new DashBoardObject(objectName, object.getDescription(),
						object.getType());
				Set<DashBoardPermission> dashBoardPermissionSet = new HashSet<>();
				dashBoardPermissionSet.add(dashBoardPermission);
				dashBoardObject.setPermission(dashBoardPermissionSet);
				dashBoardMap.put(objectName, dashBoardObject);
				continue;
			}
			DashBoardObject dashBoardObject = dashBoardMap.get(objectName);
			Set<DashBoardPermission> dashBoardPermissions = dashBoardObject.getPermission();
			dashBoardPermissions.add(dashBoardPermission);
		}
		return dashBoardMap;
	}
}

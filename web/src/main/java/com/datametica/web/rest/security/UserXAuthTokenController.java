/**
 * 
 */
package com.datametica.web.rest.security;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datametica.web.model.security.AuthenticationRequest;
import com.datametica.web.model.security.AuthenticationResponse;
import com.datametica.web.model.security.DashBoardObject;
import com.datametica.web.model.security.DashBoardUser;
import com.datametica.web.service.security.TokenUtils;

/**
 * @author Mahendra
 *
 */
@RestController
public class UserXAuthTokenController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TokenUtils tokenUtils;


	private final AuthenticationManager authenticationManager;

	@Autowired
	public UserXAuthTokenController(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> authorize(@RequestBody AuthenticationRequest authenticationRequest) {
		String userName = authenticationRequest.getUserName();
		String password = authenticationRequest.getPassword();
		log.info("UserName from UI is : {} and password from UI is : {} ", userName, password);
		Authentication authentication = this.authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
		SecurityContextHolder.getContext().setAuthentication(authentication);
	//	DashBoardUser details = (DashBoardUser) this.userDetailsService.loadUserByUsername(userName);
		DashBoardUser details =(DashBoardUser) authentication.getPrincipal();
		Set<DashBoardObject> dashBoardObjectSet=details.getDashBoardobjectSet();
		log.debug("Permissions assigned are  : {}",dashBoardObjectSet);
		return ResponseEntity
				.ok(new AuthenticationResponse(details.getUsername(), dashBoardObjectSet, tokenUtils.createToken(details)));
	}
	@RequestMapping(value="/logout",method=RequestMethod.DELETE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> logout(){
		log.info("Rest request to logout current user.");
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		tokenUtils.removeToken(userDetails);
		SecurityContextHolder.clearContext();
		return ResponseEntity.ok("Successfully logout user");
	}
	
}
package com.datametica.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datametica.businessconfigurator.model.user.BusinessRoles;
import com.datametica.businessconfigurator.model.user.BusinessUser;
import com.datametica.businessconfigurator.service.user.IRoleService;
import com.datametica.businessconfigurator.service.user.IUserService;
import com.datametica.web.model.security.DashBoardObject;
import com.datametica.web.model.security.DashBoardRole;
import com.datametica.web.model.security.DashBoardUser;


@RestController
public class UserController {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IRoleService roleService;
	

	/*@Autowired
	private PasswordEncoder passwordEncoder;*/
	
	//@PostConstruct
//	public void testPasswordEncoder(){
//		System.out.println("***************************");
//		System.out.println("Encoding password for : user1");
//		System.out.println(passwordEncoder.encode("user1"));
//		System.out.println("Successfully encoded password");
//		System.out.println("****************************");
//	}
	
	@RequestMapping(value = "/api/admin/rest/users", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getUsers(){
		log.info("Rest request to get all users in the system.");
		List<BusinessUser> users = userService.getUsers();
		List<DashBoardUser> dashBoardUserList = new ArrayList<DashBoardUser>(); 
		for(BusinessUser businessUser : users){
			DashBoardUser dashBoardUser = new DashBoardUser();
			BeanUtils.copyProperties(businessUser, dashBoardUser);
			BusinessRoles businessRole = businessUser.getBusinessRole();
			DashBoardRole dashBoardRole = new DashBoardRole(businessRole.getId(),businessRole.getName(),businessRole.getDescription());
			dashBoardUser.setDashBoardRole(dashBoardRole);
			dashBoardUserList.add(dashBoardUser);
		}
		log.debug("users in system are : {} ",dashBoardUserList);
		return ResponseEntity.ok(dashBoardUserList);
	}
	
	@RequestMapping(value="/api/admin/rest/user/permission/{username}",method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> gPermissions(@PathVariable String username){
		log.info("Request to check permissions for user : {}  ",username);
		String userNameFromContext=SecurityContextHolder.getContext().getAuthentication().getName();
		if(!userNameFromContext.equals(username)){
			log.error("username : {} is not equal with the one in context : {} ",username,userNameFromContext);
			return ResponseEntity.badRequest().body("UserName not present in context");
		}
		DashBoardUser dashBoardUser = (DashBoardUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Set<DashBoardObject> dashBoardObjects = dashBoardUser.getDashBoardobjectSet();
		return ResponseEntity.ok(dashBoardObjects);
	}
	
	@RequestMapping(value = "/api/admin/rest/user", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUser(@RequestBody DashBoardUser dashBoardUser) {
		log.info("Rest request to create dashboardUser : {} ",dashBoardUser);
		BusinessUser businessUser = new BusinessUser();
		BeanUtils.copyProperties(dashBoardUser, businessUser);
		BusinessRoles businessRole = roleService.getRole(dashBoardUser.getDashBoardRole().getName());
		businessUser.setBusinessRole(businessRole);
		businessUser.setCreatedDate(new Date());
		//String enpassword= passwordEncoder.encode(dashBoardUser.getPassword());
		//businessUser.setPassword(enpassword);
		businessUser.setLastModifiedDate(new Date());
		BusinessUser savedUser = userService.saveUser(businessUser);
		BeanUtils.copyProperties(savedUser, dashBoardUser);
		return ResponseEntity.ok(dashBoardUser);
	}
	@RequestMapping(value="/api/admin/rest/user",method = RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editUser(@RequestBody DashBoardUser dashBoardUser){
		log.info("Rest request to edit dashboardUser : {} ",dashBoardUser);
		BusinessUser businessUser=userService.getUserByUserName(dashBoardUser.getUsername());
		BusinessRoles businessRole = roleService.getRole(dashBoardUser.getDashBoardRole().getName());
		BeanUtils.copyProperties(dashBoardUser, businessUser);
		businessUser.setBusinessRole(businessRole);
		businessUser.setLastModifiedDate(new Date());
		String password=dashBoardUser.getPassword();
		//String enpassword=passwordEncoder.encode(password);
		//businessUser.setPassword(enpassword);
		BusinessUser savedUser = userService.saveUser(businessUser);
		BeanUtils.copyProperties(savedUser, dashBoardUser);
		return ResponseEntity.ok(dashBoardUser);
	}
	@RequestMapping(value="/api/admin/rest/user",method = RequestMethod.DELETE,produces=MediaType.APPLICATION_JSON_VALUE)
	public void deleteUser(@RequestBody String userName){
		log.info("Rest request to delete user : {} ",userName);
		userService.deleteUser(userName);
	}

}
/**
 * 
 */
package com.datametica.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datametica.businessconfigurator.model.user.BusinessRoles;
import com.datametica.businessconfigurator.service.user.IRoleService;
import com.datametica.web.model.security.DashBoardRole;

/**
 * @author mahendra
 *
 */
@RestController
public class RoleController {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IRoleService roleService;
	
	@RequestMapping(value = "/api/admin/rest/roles", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRoles(){
		log.info("Rest request to get all roles in system");
		List<BusinessRoles> businessRoles = roleService.getRoles();
		List<DashBoardRole> dashBoardRolesList = new ArrayList<>();
		for(BusinessRoles businessRole : businessRoles){
			dashBoardRolesList.add(new DashBoardRole(businessRole.getId(),businessRole.getName(),businessRole.getDescription()));
		}
		log.debug("All roles in system are : {} ",dashBoardRolesList);
		return ResponseEntity.ok(dashBoardRolesList);
	} 
	@RequestMapping(value = "/api/admin/rest/role", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createRole(@RequestBody DashBoardRole dashBoardRole){
		log.info("Rest request to create role  : {} ",dashBoardRole);
		BusinessRoles businessRole = new BusinessRoles();
		BeanUtils.copyProperties(dashBoardRole, businessRole);
		BusinessRoles createdRole = roleService.createRole(businessRole);
		return ResponseEntity.ok(createdRole);
	}
	
	@RequestMapping(value = "/api/admin/rest/role", method = RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editRole(@RequestBody DashBoardRole dashBoardRole){
		log.info("Rest request to edit role : {} ",dashBoardRole);
		BusinessRoles businessRole = roleService.getRole(dashBoardRole.getId());
		BeanUtils.copyProperties(dashBoardRole, businessRole);
		BusinessRoles newRole=roleService.editRole(businessRole);
		BeanUtils.copyProperties(newRole, dashBoardRole);
		log.debug("New Role after editing is : {} ",dashBoardRole);
		return ResponseEntity.ok(dashBoardRole);
	}
	
	
	@RequestMapping(value="/api/admin/rest/role",method = RequestMethod.DELETE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteRole(@RequestBody String roleName){
		log.info("Rest request to delete role : {} ",roleName);
		boolean failed=false;
		try{
			roleService.deleteRole(roleName);
			log.info("Successfully delete role : {} ",roleName);
		}catch(DataIntegrityViolationException e){
			log.error(String.format("Role : %s is already referred in user table so cannot delete this role", roleName),e);
			failed=true;
		}
		if(failed){
			return ResponseEntity.ok(HttpStatus.CONFLICT);
		}
		return ResponseEntity.ok(HttpStatus.OK);
	}
	

}

/**
 * 
 */
package com.datametica.web.configs.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.datametica.web.service.security.TokenUtils;

/**
 * @author mahendra
 *
 */
public class XAuthTokenConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
	
	
	private final TokenUtils tokenUtils;

	@Autowired
	public XAuthTokenConfigurer(TokenUtils tokenUtils) {
		this.tokenUtils=tokenUtils;
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		XAuthTokenFilter customFilter = new XAuthTokenFilter(tokenUtils);
		http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
}
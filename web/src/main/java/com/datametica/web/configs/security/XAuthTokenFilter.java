/**
 * 
 */
package com.datametica.web.configs.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.datametica.web.service.IConstants;
import com.datametica.web.service.security.TokenUtils;

/**
 * @author mahendra
 *
 */
public class XAuthTokenFilter extends GenericFilterBean {

	private final TokenUtils tokenUtils;

	@Autowired
	public XAuthTokenFilter(TokenUtils tokenUtils) {
		this.tokenUtils = tokenUtils;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String authToken = httpServletRequest.getHeader(IConstants.xAuthTokenHeaderName);

			if (StringUtils.hasText(authToken)) {
				// String username = tokenUtils.getUserNameFromToken(authToken);
				// UserDetails userDetails =
				// userDetailsService.loadUserByUsername(username);
				UserDetails userDetails = tokenUtils.getUser(authToken);
				if (tokenUtils.validateToken(authToken, userDetails)) {
					UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails,
							userDetails.getPassword(), userDetails.getAuthorities());
					SecurityContextHolder.getContext().setAuthentication(token);
				}
			}
			filterChain.doFilter(request, response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

}
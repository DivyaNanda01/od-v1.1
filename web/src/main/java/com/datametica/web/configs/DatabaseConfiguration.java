package com.datametica.web.configs;

import java.util.Arrays;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfiguration implements EnvironmentAware {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);
	private RelaxedPropertyResolver propertyResolver;
	private Environment env;

	@Override
	public void setEnvironment(Environment env) {
		this.env = env;
		this.propertyResolver = new RelaxedPropertyResolver(env, "spring.datasource.");
	}

	@Bean(destroyMethod = "shutdown")
	public DataSource dataSource() {
		HikariConfig config = new HikariConfig();
		try {
			if (propertyResolver.getProperty("url") == null && propertyResolver.getProperty("databaseName") == null) {
				log.error(
						"Your database connection pool configuration is incorrect! The application"
								+ "cannot start. Please check your Spring datasource configuration ",
						Arrays.toString(env.getActiveProfiles()));

				throw new ApplicationContextException("Database connection pool is not configured correctly");
			}
			
			config.setDriverClassName("com.mysql.jdbc.Driver");
			config.setJdbcUrl(propertyResolver.getProperty("url"));
			config.setUsername(propertyResolver.getProperty("username"));
			config.setPassword(propertyResolver.getProperty("password"));
			config.setPoolName(propertyResolver.getProperty("poolName"));
			config.addDataSourceProperty("hibernate.hbm2ddl.auto", "create-update");
		} catch (Exception e) {
			log.error("Exception occured while configuring hikari datasource : ", e);
		}
		return new HikariDataSource(config);
	}
}
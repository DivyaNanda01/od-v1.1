package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datametica.businessconfigurator.model.QueryDetails;

public interface QueryDetailsRepository extends
		JpaRepository<QueryDetails, Serializable> {

}
/**
 * 
 */
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.datametica.businessconfigurator.model.user.BusinessObject;

/**
 * @author mahedndra
 *
 */
public interface BusinessObjectRepository extends JpaRepository<BusinessObject, Serializable> {
	
	@Query("select distinct o.name from BusinessObject o")
	Set<String> findnames();
	
	BusinessObject getBusinssObjectByname(String name);
}

/**
 * 
 */
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.datametica.businessconfigurator.model.user.BusinessObject;
import com.datametica.businessconfigurator.model.user.BusinessPermission;

/**
 * @author mahendra
 *
 */
public interface BusinessPermissionRepository extends JpaRepository<BusinessPermission, Serializable> {
	
	@Query("select distinct p.name from BusinessPermission p")
	Set<String> findnames();
	
	BusinessPermission findBynameAndBusinessObject(String name,BusinessObject object);

}

/**
 * 
 */
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datametica.businessconfigurator.model.user.BusinessUser;


/**
 * @author mahendra
 *
 */
@Repository
public interface UserRepository extends JpaRepository<BusinessUser, Serializable> {
	
	BusinessUser findUserByusername(String username);
	

	void deleteByusername(String username);

}

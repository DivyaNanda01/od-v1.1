package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datametica.businessconfigurator.model.Query;

public interface QueryRepository extends JpaRepository<Query, Serializable> {

}
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datametica.businessconfigurator.model.DataSource;

public interface DataSourceRepository extends JpaRepository<DataSource, Serializable> {

}
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datametica.businessconfigurator.model.DataSourceType;

public interface DataSourceTypeRepository extends
		JpaRepository<DataSourceType, Serializable> {

}
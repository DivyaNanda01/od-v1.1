/**
 * 
 */
package com.datametica.businessconfigurator.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datametica.businessconfigurator.model.user.BusinessRoles;

/**
 * @author mahendra
 *
 */
@Repository
public interface BusinessRoleRepository extends JpaRepository<BusinessRoles,Serializable> {

	BusinessRoles findByName(String name);
	void deleteByname(String name);
}

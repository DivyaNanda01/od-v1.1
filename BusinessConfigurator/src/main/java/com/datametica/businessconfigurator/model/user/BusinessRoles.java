/**
 * 
 */
package com.datametica.businessconfigurator.model.user;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author mahendra
 *
 */
@Entity
@Table(name = "role")
public class BusinessRoles implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2309316397373769681L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private String name;
	private String description;

	@ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name = "role_permission", joinColumns = {
			@JoinColumn(name = "role_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "permission_id", referencedColumnName = "id") })
	private Set<BusinessPermission> permissions;
	
	@OneToMany(mappedBy="businessRole",fetch=FetchType.EAGER)
	private List<BusinessUser> businessUserList;
	
	public BusinessRoles() {
	}
	
	public BusinessRoles(Integer id,String name,String description){
		this.id=id;
		this.name=name;
		this.description=description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<BusinessPermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<BusinessPermission> permissions) {
		this.permissions = permissions;
	}

	public List<BusinessUser> getBusinessUserList() {
		return businessUserList;
	}

	public void setBusinessUserList(List<BusinessUser> businessUserList) {
		this.businessUserList = businessUserList;
	}
	@Override
	public String toString() {
		return "{BusinessRole : name : "+name+" description : "+description+" permissions : "+permissions+"}";
	}
}
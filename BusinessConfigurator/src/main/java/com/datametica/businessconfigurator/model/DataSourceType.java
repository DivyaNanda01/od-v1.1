package com.datametica.businessconfigurator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datametica.queryexecuter.service.IDataSource;
import com.datametica.queryexecuter.service.IDataSourceType;

@Entity
@Table(name = "datasourcetype")
public class DataSourceType implements Serializable , IDataSourceType{

	private static final long serialVersionUID = -194729854368847567L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "datasourcetypeid", unique = true)
	private Integer id;
	
	@Column(name = "name", unique = true)
	private String name;
	
	@OneToMany(mappedBy="dataSourceType",fetch=FetchType.EAGER)
	private List<DataSource> dataSourceList;
	
	public Integer getDataSourceTypeId() {
		return id;
	}

	public void setDataSourceTypeId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {

		return id + "\t" + name;
	}

	@Override
	public List<IDataSource> getIDataSourceList() {
		List<IDataSource> idataSourceList = new ArrayList<>();
		List<? extends IDataSource> iQueryDetailsList = dataSourceList;
		return (List<IDataSource>)iQueryDetailsList;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<DataSource> getDataSourceList() {
		return dataSourceList;
	}

	public void setDataSourceList(List<DataSource> dataSourceList) {
		this.dataSourceList = dataSourceList;
	}
}

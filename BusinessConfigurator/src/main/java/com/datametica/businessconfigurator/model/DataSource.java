package com.datametica.businessconfigurator.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datametica.queryexecuter.service.IDataSource;
import com.datametica.queryexecuter.service.IDataSourceConnectionDetails;
import com.datametica.queryexecuter.service.IQuery;

@Entity
@Table(name = "datasource")
public class DataSource implements Serializable, IDataSource {

	private static final long serialVersionUID = -194729854368847567L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "dataSource_id")
	private List<Query> queryList;

	@OneToMany(mappedBy="dataSource",fetch=FetchType.EAGER)
	private List<DataSourceConnectionDetails> dataSourceConnectionDetails;
	
	@ManyToOne
	@JoinColumn(name = "datasource_type_id", nullable = false)
	private DataSourceType dataSourceType;

	public DataSourceType getDataSourceType() {
		return dataSourceType;
	}

	public void setDataSourceType(DataSourceType dataSourceType) {
		this.dataSourceType = dataSourceType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Query> getQueryList() {
		return queryList;
	}

	public void setQueryList(List<Query> queryList) {
		this.queryList = queryList;
	}

	public List<DataSourceConnectionDetails> getDataSourceConnectionDetails() {
		return dataSourceConnectionDetails;
	}

	public void setDataSourceConnectionDetails(
			List<DataSourceConnectionDetails> dataSourceConnectionDetails) {
		this.dataSourceConnectionDetails = dataSourceConnectionDetails;
	}

	@Override
	public Integer getDataSourceId() {
		return id;
	}

	@Override
	public List<IQuery> getIQueryList() {
		List<? extends IQuery> iQueryList = queryList;
		return (List<IQuery>)iQueryList;
	}

	@Override
	public List<IDataSourceConnectionDetails> getIDataSourceConnectionDetails() {
		List<? extends IDataSourceConnectionDetails> iQueryDetailsList = dataSourceConnectionDetails;
		return (List<IDataSourceConnectionDetails>)iQueryDetailsList;
	}


	

}

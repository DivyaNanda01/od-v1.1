/**
 * 
 */
package com.datametica.businessconfigurator.model.user;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author mahendra
 *
 */
@Entity
@Table(name = "permission")
public class BusinessPermission implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6009920092537684650L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;
	private String description;

	@ManyToMany(mappedBy = "permissions")
	private Set<BusinessRoles> roles;

	@ManyToOne
	@JoinColumn(name = "object_id", nullable = false)
	private BusinessObject businessObject;

	
	public BusinessPermission() {
	}
	
	public BusinessPermission(String name,String description){
		this.name=name;
		this.description=description;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<BusinessRoles> getRoles() {
		return roles;
	}

	public void setRoles(Set<BusinessRoles> roles) {
		this.roles = roles;
	}

	public BusinessObject getObject() {
		return businessObject;
	}

	public void setObject(BusinessObject object) {
		this.businessObject = object;
	}

	@Override
	public String toString() {
		return "{BusinessPermissions : name:"+name+" descriptions : "+description+" object : "+businessObject+"}";
	}

}
package com.datametica.businessconfigurator.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.cache.annotation.Cacheable;

import com.datametica.queryexecuter.service.IQuery;
import com.datametica.queryexecuter.service.IQueryDetails;

@Entity
@Table(name = "query")
@Cacheable("query")
public class Query implements Serializable, IQuery {

	private static final long serialVersionUID = -3940169416746357513L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;

	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "datasource_id", nullable = false)
	private DataSource dataSource_id;

	@OneToMany(mappedBy = "query", fetch = FetchType.EAGER)
	private List<QueryDetails> queryDetailsList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DataSource getDataSource() {
		return dataSource_id;
	}

	public void setDataSource(DataSource dataSource_id) {
		this.dataSource_id = dataSource_id;
	}

	public void setQueryDetailsList(List<QueryDetails> queryDetailsList) {
		this.queryDetailsList = queryDetailsList;
	}

	@Override
	public List<IQueryDetails> getIQueryDetailsList() {
		List<? extends IQueryDetails> iQueryDetailsList = queryDetailsList;
		return (List<IQueryDetails>) iQueryDetailsList;
	}

}

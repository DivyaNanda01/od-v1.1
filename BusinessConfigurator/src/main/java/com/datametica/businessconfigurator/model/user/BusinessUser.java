/**
 * 
 */
package com.datametica.businessconfigurator.model.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author mahendra
 *
 */
@Entity
@Table(name="user")
public class BusinessUser  implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 993526773426912160L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private String username;
	private String password;
	private boolean enabled;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email_addr")
	private String email;
	@Column(name="created_by")
	private String createdBy;
	@Column(name="creation_time")
	private Date createdDate;
	@Column(name="last_modified_by")
	private String lastModifiedBy;
	@Column(name="last_modification_time")
	private Date lastModifiedDate;
	
	@ManyToOne
	@JoinColumn(name="role_id")
	private BusinessRoles businessRole;

	public BusinessUser() {
	}

	@Override
	public String toString() {
		return "{enabled:" + enabled + ", username:'" + username + "', password:'" + password + "'}";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public BusinessRoles getBusinessRole() {
		return businessRole;
	}

	public void setBusinessRole(BusinessRoles businessRole) {
		this.businessRole = businessRole;
	}
	
}
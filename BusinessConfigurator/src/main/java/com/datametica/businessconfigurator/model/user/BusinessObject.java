/**
 * 
 */
package com.datametica.businessconfigurator.model.user;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author mahendra
 *
 */
@Entity
@Table(name="object")
public class BusinessObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7569352308001889713L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String name;
	private String description;
	private String type;
	
	@OneToMany(mappedBy="businessObject",fetch=FetchType.EAGER)
	private Set<BusinessPermission> permission;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Set<BusinessPermission> getPermission() {
		return permission;
	}
	public void setPermission(Set<BusinessPermission> permission) {
		this.permission = permission;
	}
	
	@Override
	public String toString() {
		return "{BusinessObject : name :"+name+" description : "+description+" type : "+type+"}";
	}

}
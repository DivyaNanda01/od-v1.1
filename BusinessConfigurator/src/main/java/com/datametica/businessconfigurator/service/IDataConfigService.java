package com.datametica.businessconfigurator.service;

import com.datametica.businessconfigurator.model.Query;

public interface IDataConfigService {
	Query getQueryByName(String queryName) throws Exception ;

}

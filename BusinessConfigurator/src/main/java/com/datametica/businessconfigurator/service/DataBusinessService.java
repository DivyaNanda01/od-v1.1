package com.datametica.businessconfigurator.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.Query;
import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IQueryService;

@Service
public class DataBusinessService implements IDataBusinessService {

	String queryName = null;
	@Autowired
	IDataConfigService dataConfigService;
	@Autowired
	IQueryService queryService;
	

	public List<Map<String, Object>> getData(String queryName, String filterConditions,
			Map<String, String> userDetails) throws Exception {
			Query query = ((DataConfigService) dataConfigService).getQueryByName(queryName);

			QueryConfig queryConfig = new QueryConfig(query, filterConditions, userDetails);

			return queryService.getData(queryConfig);

	}
}
package com.datametica.businessconfigurator.service.user;

import java.util.Set;

import com.datametica.businessconfigurator.model.user.BusinessObject;
import com.datametica.businessconfigurator.model.user.BusinessPermission;

public interface IBusinessPermission {

	Set<String> getAllPermissions();
	
	BusinessPermission getBusinessPermissionBynameAndobject(String name,BusinessObject object);

}

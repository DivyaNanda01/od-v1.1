package com.datametica.businessconfigurator.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.NotFoundException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.DataSource;
import com.datametica.businessconfigurator.model.DataSourceConnectionDetails;
import com.datametica.businessconfigurator.model.DataSourceType;
import com.datametica.businessconfigurator.model.Query;
import com.datametica.businessconfigurator.model.QueryDetails;
import com.datametica.businessconfigurator.repository.DataSourceConnectionDetailsRepository;
import com.datametica.businessconfigurator.repository.DataSourceRepository;
import com.datametica.businessconfigurator.repository.DataSourceTypeRepository;
import com.datametica.businessconfigurator.repository.QueryDetailsRepository;
import com.datametica.businessconfigurator.repository.QueryRepository;

@Service
public class DataConfigService implements IDataConfigService {
	public static Map<String, Query> queryMap = new HashMap<>();

	@Autowired
	private QueryRepository queryRepo;
	@Autowired
	private QueryDetailsRepository queryDetailsRepo;
	@Autowired
	private DataSourceRepository dataSourceRepo;
	@Autowired
	private DataSourceConnectionDetailsRepository dataSourceConnectionDetailsRepo;
	@Autowired
	private DataSourceTypeRepository dataSourceTypeRepo;

	List<Query> queryList = new ArrayList<>();
	List<QueryDetails> queryDetailsList = new ArrayList<>();
	List<DataSource> dataSourceList = new ArrayList<>();
	List<DataSourceConnectionDetails> dataSourceConnectionDetailsList = new ArrayList<>();
	List<DataSourceType> dataSourceTypeList = new ArrayList<>();

	public List<Query> getQueryList() {
		return queryRepo.findAll();
	}

	@PostConstruct
	public void loadQueries() {
		queryList = queryRepo.findAll();
		queryDetailsList = queryDetailsRepo.findAll();
		dataSourceList = dataSourceRepo.findAll();
		dataSourceConnectionDetailsList = dataSourceConnectionDetailsRepo.findAll();
		dataSourceTypeList = dataSourceTypeRepo.findAll();

		for (Query query : queryList) {
			queryMap.put(query.getName(), query);
		}
	}

	public Query getQueryByName(String queryName) throws Exception {
		if (queryMap.containsKey(queryName)) {
			return queryMap.get(queryName);
		} else {
			throw new NotFoundException("the value of queryname: " + queryName
					+ " is not found in the table: 'query'. possible values are: " + queryMap.keySet());
		}

	}

}

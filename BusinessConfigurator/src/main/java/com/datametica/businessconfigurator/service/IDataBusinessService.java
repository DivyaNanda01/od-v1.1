package com.datametica.businessconfigurator.service;

import java.util.List;
import java.util.Map;

public interface IDataBusinessService {

	public List<Map<String, Object>> getData(String queryName,
			String filterConditions, Map<String, String> userDetails) throws Exception;

	
}

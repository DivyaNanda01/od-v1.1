/**
 * 
 */
package com.datametica.businessconfigurator.service.user;

import java.util.List;

import com.datametica.businessconfigurator.model.user.BusinessUser;

/**
 * @author mahendra
 *
 */
public interface IUserService {
	BusinessUser getUserByUserName(String userName);
	List<BusinessUser> getUsers();
	BusinessUser saveUser(BusinessUser businessUser);
	void deleteUser(String userName);
}

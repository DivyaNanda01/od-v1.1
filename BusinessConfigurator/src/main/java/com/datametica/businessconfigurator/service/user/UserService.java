/**
 * 
 */
package com.datametica.businessconfigurator.service.user;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.user.BusinessUser;
import com.datametica.businessconfigurator.repository.UserRepository;


/**
 * @author mahendra
 *
 */
@Service
public class UserService implements IUserService{
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private final UserRepository userRepository;
	
	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository=userRepository;
	}
	
	@Override
	public BusinessUser getUserByUserName(String userName){
		log.info("Finding whether user with name : {} exists or not.",userName);
		return userRepository.findUserByusername(userName);
	}

	@Override
	public List<BusinessUser> getUsers() {
		return userRepository.findAll();
	}
	
	@Override
	public BusinessUser saveUser(BusinessUser user){
		userRepository.save(user);
		return user;
	}

	@Override
	public void deleteUser(String userName) {
		BusinessUser user =userRepository.findUserByusername(userName);
		//userRepository.deleteByusername(userName);
		userRepository.delete(user);
		log.info("Successfully delete user : {} ",userName);
	}
	
	
	
	
	


}

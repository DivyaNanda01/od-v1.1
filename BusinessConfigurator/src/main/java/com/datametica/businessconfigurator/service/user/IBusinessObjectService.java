/**
 * 
 */
package com.datametica.businessconfigurator.service.user;

import java.util.List;
import java.util.Set;

import com.datametica.businessconfigurator.model.user.BusinessObject;

/**
 * @author mahendra
 *
 */
public interface IBusinessObjectService {

	Set<String> getBusinessObjectNames();
	BusinessObject getBusinessObject(String businessObjectName);
	List<BusinessObject> getBusinessObjects();

}

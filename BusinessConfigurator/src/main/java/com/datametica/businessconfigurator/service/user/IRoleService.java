/**
 * 
 */
package com.datametica.businessconfigurator.service.user;

import java.util.List;

import com.datametica.businessconfigurator.model.user.BusinessRoles;

/**
 * @author mahendra
 *
 */
public interface IRoleService {
	
	BusinessRoles getRole(String roleName);
	List<BusinessRoles> getRoles();
	BusinessRoles getRole(Integer roleId);
	BusinessRoles createRole(BusinessRoles role);
	BusinessRoles editRole(BusinessRoles role);
	void deleteRole(String roleName);
	
	

}

/**
 * 
 */
package com.datametica.businessconfigurator.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.user.BusinessRoles;
import com.datametica.businessconfigurator.repository.BusinessRoleRepository;

/**
 * @author mahendra
 *
 */
@Service
public class RoleService implements IRoleService {

	private final BusinessRoleRepository roleRepository;

	@Autowired
	public RoleService(BusinessRoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public BusinessRoles getRole(String roleName) {
		return roleRepository.findByName(roleName);
	}

	@Override
	public List<BusinessRoles> getRoles() {
		return roleRepository.findAll();
	}

	@Override
	public BusinessRoles getRole(Integer roleId) {
		return roleRepository.getOne(roleId);
	}

	@Override
	public BusinessRoles editRole(BusinessRoles role) {
		return roleRepository.save(role);
	}

	@Override
	public void deleteRole(String roleName) {
		BusinessRoles role = roleRepository.findByName(roleName);
		roleRepository.delete(role);
	}

	@Override
	public BusinessRoles createRole(BusinessRoles role) {
		return roleRepository.save(role);
	}
}

package com.datametica.businessconfigurator.service.user;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.user.BusinessObject;
import com.datametica.businessconfigurator.repository.BusinessObjectRepository;

/**
 * @author mahendra
 *
 */
@Service
public class BusinessObjectService implements IBusinessObjectService {

	@Autowired
	private BusinessObjectRepository objectRepository;

	@Override
	public Set<String> getBusinessObjectNames() {
		return objectRepository.findnames();
	}

	@Override
	public BusinessObject getBusinessObject(String businessObjectName) {
		return objectRepository.getBusinssObjectByname(businessObjectName);
	}

	@Override
	public List<BusinessObject> getBusinessObjects() {
		return objectRepository.findAll();
	}

}

package com.datametica.businessconfigurator.service.user;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.businessconfigurator.model.user.BusinessObject;
import com.datametica.businessconfigurator.model.user.BusinessPermission;
import com.datametica.businessconfigurator.repository.BusinessPermissionRepository;

/**
 * @author mahendra
 *
 */
@Service
public class BusinessPermissionService implements IBusinessPermission{
	
	@Autowired
	private BusinessPermissionRepository permissionRepository;

	
	@Override
	public Set<String> getAllPermissions(){
		return permissionRepository.findnames();
	}


	@Override
	public BusinessPermission getBusinessPermissionBynameAndobject(String name, BusinessObject object) {
		return permissionRepository.findBynameAndBusinessObject(name, object);
	}

}

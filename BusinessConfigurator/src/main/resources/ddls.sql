SET foreign_key_checks = 0;
create database if not exists test;
use test;


drop table if exists datasource;
create table datasource(
id int not null,
datasource_type_id int not null,
name varchar(250 ),
description varchar(250),
primary key(id),
foreign key(id) references query(datasource_id),
foreign key(id) references datasource_connection_details(data_source_id),
foreign key(id) references datasourcetype(datasourcetypeid)
);


drop table  if exists  datasource_connection_details;
create table datasource_connection_details(
id int not null,
data_source_id int(11) not null,
property_name varchar(250),
property_value varchar(250),
primary key(id),
foreign key(data_source_id) references datasource(id)
);


drop table if exists  datasourcetype;
create table datasourcetype(
datasourcetypeid int not null,
name varchar(250),
primary key(datasourcetypeid),
foreign key(datasourcetypeid) references datasource(id)
);

drop table if  exists query;
create table query(
id int not null,
datasource_id int not null,
name varchar(250),
description varchar(250),
primary key(id),
foreign key(id) references querydetails(id),
foreign key(datasource_id) references datasource(id));


drop table if exists querydetails;
create table  querydetails(
id int not null,
query_id int not null,
property_name varchar(250),
property_value varchar(2500),
primary key(id),
foreign key(query_id) references query(id)
);

drop table if exists test;
create table test(
id int,
name varchar(100),
value int,
primary key(id)
);

drop table if exists sinarmus_test;
create table sinarmus_test (
PETAK_ID varchar(300),HARVESTING_COMPLETED varchar(300),START_DATE varchar(300),END_DATE varchar(300),ACT_WOOD_POTENCY varchar(300),TRGT_WOOD_POTENCY varchar(300),TOTAL_COST varchar(300),CONTRACTOR varchar(300),TYPE_OF_WORK varchar(300),DURATION varchar(300),TONNAGE varchar(300),AVG_COST_PER_TONNAGE varchar(300),WOOD_POTENCY_DIFF varchar(300)
);


insert into 
query
values 
(1,1,'samplequery', 'sampledescription111'),
(2,2,'hivesamplequery', 'sampledescription'),
(3,3,'sinarmussamplequery', 'sinarmus description')
;

insert into 
querydetails
values 
(1,1,'query-str', 'select * from test where 1 = 1'),
(2,2,'query-str', 'select * from jdbc_test where 1 = 1'),
(3,3,'query-str', 'select PETAK_ID  as PETAK_ID,HARVESTING_COMPLETED  as HARVESTING_COMPLETED ,START_DATE as START_DATE ,END_DATE as END_DATE,ACT_WOOD_POTENCY as ACT_WOOD_POTENCY,TRGT_WOOD_POTENCY as TRGT_WOOD_POTENCY,TOTAL_COST as TOTAL_COST,CONTRACTOR as CONTRACTOR ,TYPE_OF_WORK as TYPE_OF_WORK,DURATION as DURATION,TONNAGE as TONNAGE,AVG_COST_PER_TONNAGE as AVG_COST_PER_TONNAGE,WOOD_POTENCY_DIFF as WOOD_POTENCY_DIFF from sinarmus_test where 1 = 1 ;')
;

insert into 
datasourcetype
values 
(1,'aaaaaaa'),
(2, 'bbbbb'),
(3, 'ccccc')
;

insert into 
datasource_connection_details
values 
(1,1,'driver-class', 'com.mysql.jdbc.jdbc2.optional.MysqlDataSource'),
(2,1,'url', 'jdbc:mysql://10.200.100.21:3306/test'),
(3,1,'username', 'root'),
(4,1,'password', 'redhat'),
(5,2,'driver-class', 'org.apache.hive.jdbc.HiveDriver'),
(6,2,'url', 'jdbc:hive2://10.200.99.111:10000/default'),
(7,2,'username', 'root'),
(8,2,'password', ''),
(9,3,'username', 'root'),
(10,3,'password', ''),
(11, 3, 'driver-class', 'org.apache.hive.jdbc.HiveDriver'),
(12, 3, 'url', 'jdbc:hive2://10.200.99.111:10000/default')
;

insert into 
datasource
values 
(1,1,'samplename', 'sampledescription'),
(2,2,'hive-samplename', 'hive-sampledescription'),
(3,3,'sinarmus-samplename', 'sinarmus-sampledescription')
;

insert into test
values
(1,'name1', 10),(2,'name2',80),(3,'name3',10)
;


insert into  sinarmus_test values ('JAMBI','DA','JAMBI','BKB0003000','20151002','1','30/04/2015','29/06/2015','76.98','50.89','147148872','90008910','Kebersihan Lahan Flat\;Slashing (Kebersihan Lahan)\;Tebang\;Loading to Truck HTI\;Extraction');
insert into  sinarmus_test values
('JAMBI','DA','JAMBI','BKB0003001','20151002','1','07/05/2015','06/07/2015','77.89','95.71','141099426','90008910','Kebersihan Lahan\;Slashing (Kebersihan Lahan)\;Tebang\;Loading to Truck HTI\;Extraction');
insert into  sinarmus_test values
('JAMBI','DA','JAMBI','TPH0005000','20151002','1','12/05/2015','11/07/2015','176.92','181.477','234350303','90001569','Tebang\;Extraction\;Loading to Truck HTI\;Kebersihan Lahan\;Slashing (Kebersihan Lahan)');
insert into  sinarmus_test values
('JAMBI','DA','JAMBI','TPH0004701','20151002','0','12/10/2015','11/12/2015','203.85','197.47','348853923','90001569','Tebang\;Extraction\;Loading to Sampan Besi /dgn Estafet HTI\;Loading to Truck HTI\;Kebersihan Lahan\;Slashing (Kebersihan Lahan)');
insert into  sinarmus_test values
('JAMBI','DA','JAMBI','TPH0027403','20151002','0','26/09/2015','25/11/2015','59.18','74.88','91918807','90008588','Tebang\;Extraction\;Loading to Truck HTI\;Kebersihan Lahan\;Slashing (Kebersihan Lahan)');



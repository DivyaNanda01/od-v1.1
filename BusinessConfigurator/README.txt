
DataConfigService loads all local tables into corresponding objects. 
The getQueryByName requires queryname and returns the query string correponding to this queryname.


Following are the local tables. 
user should update these tables for any new quey or datasource that needs to be configured

insert into QUERY values 
(1,1,'samplequery', 'sampledescription111');

insert into QUERYDETAILS values 
(1,1,'query-str', 'select * from test where 1 = 1');

insert into DATASOURCETYPE values
(1,'sampledatasource');

insert into DATASOURCE_CONNECTION_DETAILS values 
(1,1,'driver-class', 'com.mysql.jdbc.jdbc2.optional.MysqlDataSource'),
(2,1,'url', 'jdbc:mysql://10.200.100.21:3306/test'),
(3,1,'username', 'root'),
(4,1,'password', 'redhat');

insert into DATASOURCE values 
(1,1,'samplename', 'sampledescription');
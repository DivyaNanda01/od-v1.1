Naming conventions to follow while passing parameters to query executor:

Query details parameter:
	query-str will be the property key for the SQL query template.
	
Data source details/ connection details parameters
-	In case the data source represents one SQL data source to be looked up from JNDI context then the name of that data source
should be prefixed by "jndi-resource, e.g. jndi-resource:ds1".
-	Individual data source connection details parameter should follow the following convention.
		*	"jndi-resource" should be provided as property key for data source to be looked up using JNDI context.
		*	"driver-class" should be provided as property key for JDBC driver class.
		*	"url" is the property key for data source URL.
		*	"username" and "password", respectively to be provided as property key for database user name and password.
				 	
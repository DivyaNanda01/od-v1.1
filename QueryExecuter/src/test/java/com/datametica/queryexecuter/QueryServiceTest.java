package com.datametica.queryexecuter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IDataSource;
import com.datametica.queryexecuter.service.IDataSourceConnectionDetails;
import com.datametica.queryexecuter.service.IQuery;
import com.datametica.queryexecuter.service.IQueryDetails;
import com.datametica.queryexecuter.service.IQueryService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class QueryServiceTest {

	public static SimpleNamingContextBuilder namingContextBuilder;
	public static boolean namingContextActive = false;
	public static String driverClass = "org.hsqldb.jdbc.JDBCDriver";
	public static String url = "jdbc:hsqldb:mem:hSQLDataSource";
	public static String userName = "sa";
	public static String password = "";
	public static String dsName = "java:/my-ds";
	public static String invalidClass = "com.mysql.jdbc.InvalidDriver";
	public static String invalidDS = "java:/my-ds-does-not-exist";
	public static String query = "select * from products where 1 = 1";
	public static String filter = "id > 0";
	private SimpleQuery simpleQuery = new SimpleQuery();
	
	public static boolean isNamingContextActive() {
		return namingContextActive;
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("Beans-test.xml");
		namingContextBuilder = new SimpleNamingContextBuilder();
		namingContextBuilder.bind(dsName, appContext.getBean("hSQLDataSource"));
		namingContextBuilder.activate();
		namingContextActive = true;		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(isNamingContextActive()){
			namingContextBuilder.deactivate();
			namingContextActive = false;
		}
	}

	@Before
	public void setUp() throws Exception {
		userDetails.put("username", userName);
		userDetails.put("password", password);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJNDIDataSource() {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("jndi-resource:myds");
		simpleQuery.clearConnectionDetails();
		simpleQuery.addConnectionDetail("jndi-resource", dsName);
		QueryConfig config = new QueryConfig(simpleQuery, filter, null);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(System.err, data);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			assertNotEquals(data, null);
			assertNotEquals(data.size(), 0);
		} catch (Exception e1) {
			fail(e1.getMessage());
		}
	}

	@Test
	public void testJNDIDataSourceDSNotFound() {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("jndi-resource:mydsdoesnotexist");
		simpleQuery.clearConnectionDetails();
		simpleQuery.addConnectionDetail("jndi-resource", invalidDS);
		QueryConfig config = new QueryConfig(simpleQuery, filter, null);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			fail("Exception should have been throw as data source does not exist");
		} catch (Exception e1) {
			System.err.println("Exception correctly thrown: " + e1.toString());
		}
	}
	
	@Test
	public void testJNDIDataSourceDSNotProvided() {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("jndi-resource:mydsnotprovided");
		simpleQuery.clearConnectionDetails();
		QueryConfig config = new QueryConfig(simpleQuery, filter, null);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			fail("Exception should have been throw as data source not provided");
		} catch (Exception e1) {
			System.err.println("Exception correctly thrown: " + e1.toString());
		}
	}
	
	@Test
	public void testDataSource(){
		@SuppressWarnings("resource")
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("ds1");
		simpleQuery.clearConnectionDetails();
		simpleQuery.addConnectionDetail("driver-class", driverClass);
		simpleQuery.addConnectionDetail("url", url);
		QueryConfig config = new QueryConfig(simpleQuery, filter, userDetails);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(System.err, data);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}		
			
			assertNotEquals(data, null);
			assertNotEquals(data.size(), 0);
		} catch (Exception e1) {
			e1.printStackTrace();
			fail(e1.getMessage());
		}
	}
	
	@Test
	public void testDataSourceInvalidClass(){
		@SuppressWarnings("resource")
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("ds1-invalid-class");
		simpleQuery.addConnectionDetail("driver-class", invalidClass);
		simpleQuery.addConnectionDetail("url", url);
		QueryConfig config = new QueryConfig(simpleQuery, filter, userDetails);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			fail("Should have thrown excpetion due to invalid driver");
		} catch (Exception e1) {
			System.err.println("Exception correctly thrown: " + e1.toString());
		}
	}
	
	@Test
	public void testDataSourceInsufficientParams(){
		@SuppressWarnings("resource")
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("Beans.xml");
		IQueryService queryService = (IQueryService) context.getBean("queryService");
		simpleQuery.setDsName("ds1-insufficient-params");
		simpleQuery.addConnectionDetail("driver-class", driverClass);
		QueryConfig config = new QueryConfig(simpleQuery, filter, userDetails);
		List<Map<String, Object>> data;
		try {
			data = queryService.getData(config);
			fail("Exception should have been thrown with incorrect params");
		} catch (Exception e1) {
			System.err.println("Exception correctly thrown: " + e1.getMessage());
		}
	}
	
	Map<String, String> userDetails = new HashMap<>();
}

class SimpleQuery implements IQuery{

	@Override
	public Integer getId() {
		return 1;
	}

	@Override
	public String getName() {
		return "select";
	}

	List<IDataSourceConnectionDetails> connDetails = new ArrayList<>();
	protected String dsName;
	
	public void setDsName(String dsName){
		this.dsName = dsName;
	}
	
	public void addConnectionDetail(String key, String value){
		connDetails.add(new DataSourceConnectionDetails(key, value));
	}
	
	public void clearConnectionDetails(){
		connDetails.clear();
	}
	
	@Override
	public IDataSource getDataSource() {
		return new IDataSource() {
			
			@Override
			public String getName() {
				return dsName;
			}
			
			@Override
			public List<IQuery> getIQueryList() {
				return null;
			}
			
			@Override
			public String getDescription() {
				return null;
			}
			
			@Override
			public Integer getDataSourceId() {
				return 1;
			}
		
			@Override
			public List<IDataSourceConnectionDetails> getIDataSourceConnectionDetails() {
				return connDetails;
			}
		};
	}
	
	class DataSourceConnectionDetails implements IDataSourceConnectionDetails{

		private String name;
		private String value;

		public DataSourceConnectionDetails(String name, String value){
			this.name = name;
			this.value = value;
		}
		
		@Override
		public Integer getId() {
			return null;
		}

		@Override
		public IDataSource getDataSource() {
			return null;
		}

		@Override
		public String getPropertyName() {
			return name;
		}

		@Override
		public String getPropertyValue() {
			return value;
		}
	}
	
	@Override
	public List<IQueryDetails> getIQueryDetailsList() {
		List<IQueryDetails> queryDetails = new ArrayList<>();
		queryDetails.add(queryDetail);
		return queryDetails;
	}

	IQueryDetails queryDetail = new IQueryDetails() {
		
		@Override
		public IQuery getQuery() {
			return null;
		}
		
		@Override
		public String getPropertyName() {
			return "query-str";
		}
		
		@Override
		public String getPropertyValue() {
			return QueryServiceTest.query;
		}
		
		@Override
		public Integer getId() {
			return 1;
		}
	}; 
}

package com.datametica.queryexecuter;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;

public class DataSourceFactory {
	
	
	public DataSource getDataSourceInstance(String dataSourceType){
		
		return null;
	}

	
	String userName="ecatuser";
	String password="ecatuser";
	String databaseUrl="jdbc:mysql://10.200.100.21:3306/testdevtools";
	String driverClassName="com.mysql.jdbc.Driver";
	
	
	
	
	@Bean
	public DataSource getDataSource(){
		BasicDataSource dataSource  = new BasicDataSource();
		dataSource.setUsername(userName);
		dataSource.setPassword(password);
		dataSource.setUrl(databaseUrl);
		dataSource.setDriverClassName(driverClassName);
		return dataSource;
	}
}

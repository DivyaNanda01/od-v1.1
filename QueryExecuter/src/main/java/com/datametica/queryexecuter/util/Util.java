package com.datametica.queryexecuter.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datametica.queryexecuter.service.IPropertyDetail;

public class Util {
	public static Map<String, String> getPropertyDetailsAsMap(List<? extends IPropertyDetail> propertyDetails){
		Map<String,String> propertyDetailsMap = new HashMap<>();
		
		if(propertyDetails != null){
			for(IPropertyDetail propertyDetail : propertyDetails){
				propertyDetailsMap.put(
						propertyDetail.getPropertyName(), 
						propertyDetail.getPropertyValue());
			}
		}
		
		return propertyDetailsMap;
	}
}

package com.datametica.queryexecuter.model;

import java.util.Map;

import com.datametica.queryexecuter.service.IQuery;

public class QueryConfig {

	private IQuery query;

	private String filter;

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public Map<String, String> getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(Map<String, String> userDetails) {
		this.userDetails = userDetails;
	}

	public IQuery getQuery() {
		return query;
	}

	private Map<String, String> userDetails;

	public QueryConfig(IQuery query, String filter, Map<String, String> userDetails) {
		this.query = query;
		this.filter = filter;
		this.userDetails = userDetails;
	}

}

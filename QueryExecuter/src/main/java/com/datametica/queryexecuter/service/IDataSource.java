/**
 * 
 */
package com.datametica.queryexecuter.service;

import java.util.List;

/**
 * @author mahendra.tonape
 *
 */
public interface IDataSource {
	
	Integer getDataSourceId();
	String getName();
	String getDescription();
	List<IQuery> getIQueryList();
	List<IDataSourceConnectionDetails> getIDataSourceConnectionDetails();
}

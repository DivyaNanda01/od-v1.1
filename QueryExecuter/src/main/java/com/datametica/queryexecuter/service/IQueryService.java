package com.datametica.queryexecuter.service;

import java.util.List;
import java.util.Map;

import com.datametica.queryexecuter.model.QueryConfig;

/**
 * @author mahendra.tonape
 *
 */
public interface IQueryService {
	/**
	 * @param queryConfig
	 * @return
	 */
	public List<Map<String,Object>> getData(QueryConfig queryConfig) throws Exception;
}

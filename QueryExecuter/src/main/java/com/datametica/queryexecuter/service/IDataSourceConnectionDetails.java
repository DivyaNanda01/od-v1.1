package com.datametica.queryexecuter.service;


/**
 * @author mahendra.tonape
 *
 */
public interface IDataSourceConnectionDetails extends IPropertyDetail{
	
	Integer getId();
	IDataSource getDataSource();
}

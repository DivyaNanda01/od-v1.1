/**
 * 
 */
package com.datametica.queryexecuter.service;

import com.datametica.queryexecuter.model.QueryConfig;

/**
 * @author mahendra.tonape
 *
 */
public interface IQueryParser {
	/**
	 * Parses the query as per provided query configuration. Also the
	 * filter criteria could be provided that will replace the placeholder ?=?.
	 * @param queryConfig
	 * @return Constructed query
	 */
	String getQuery(QueryConfig queryConfig);
}

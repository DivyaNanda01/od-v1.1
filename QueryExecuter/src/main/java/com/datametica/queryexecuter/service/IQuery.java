/**
 * 
 */
package com.datametica.queryexecuter.service;

import java.util.List;


/**
 * @author mahendra.tonape
 *
 */
public interface IQuery {
	
	Integer getId();
	String getName();
	IDataSource getDataSource();
	List<IQueryDetails> getIQueryDetailsList();
}

package com.datametica.queryexecuter.service.impl;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IDataSource;
import com.datametica.queryexecuter.service.IDataSourceManager;
import com.datametica.queryexecuter.service.IQueryManager;
import com.datametica.queryexecuter.service.IQueryParser;

@Service
public class QueryManager implements IQueryManager {
	private final Logger logger = LoggerFactory.getLogger(QueryManager.class);

	@Autowired
	private IQueryParser queryParser;

	@Autowired
	private IDataSourceManager dataSourceManager;

	@Override
	public String getQuery(QueryConfig queryConfig) {
		String query = queryParser.getQuery(queryConfig);
		return query;

	}

	@Override
	public DataSource getDatasource(QueryConfig queryConfig) {
		IDataSource dataSource = getDataSource(queryConfig);
		DataSource sqlDataSource = dataSourceManager.getDataSource(dataSource, queryConfig.getUserDetails());
		return sqlDataSource;
	}

	@Override
	public List<Map<String, Object>> runQuery(QueryConfig queryConfig) {
		List<Map<String, Object>> result = null;
		IDataSource dataSource = getDataSource(queryConfig);
		DataSource sqlDataSource = dataSourceManager.getDataSource(dataSource, queryConfig.getUserDetails());

		String query = queryParser.getQuery(queryConfig);

		if (result == null) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(sqlDataSource);
			result = jdbcTemplate.queryForList(query);
		}

		return result;
	}

	private IDataSource getDataSource(QueryConfig queryConfig) {
		IDataSource datasource = queryConfig.getQuery().getDataSource();
		return datasource;
	}

	public void setQueryParser(IQueryParser queryParser) {
		this.queryParser = queryParser;
	}

	public void setDataSourceManager(IDataSourceManager dataSourceManager) {
		this.dataSourceManager = dataSourceManager;
	}
}

class DSRouteBuilder extends RouteBuilder {

	private String sql;

	private String dsName;

	private List<Map<String, Object>> data;

	@SuppressWarnings("unchecked")
	@Override
	public void configure() throws Exception {
		from("direct:myTable").setBody(constant(sql)).to("jdbc:" + dsName).process(exchange -> {
			data = (List<Map<String, Object>>) exchange.getIn().getBody();
		});

	}

	public DSRouteBuilder setSql(String sql) {
		this.sql = sql;
		return this;
	}

	public DSRouteBuilder setDsName(String dsName) {
		this.dsName = dsName;
		return this;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

}

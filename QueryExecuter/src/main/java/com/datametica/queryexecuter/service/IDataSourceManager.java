package com.datametica.queryexecuter.service;

import java.util.Map;

import javax.sql.DataSource;

public interface IDataSourceManager {
	/**
	 * Returns data source based on data source parameters. Data source can be
	 * one in a naming registry or can be one that needs to be created based on params.
	 * @param dataSource
	 * @param userDetails
	 * @return
	 */
	public DataSource getDataSource(IDataSource dataSource, Map<String, String> userDetails);
}

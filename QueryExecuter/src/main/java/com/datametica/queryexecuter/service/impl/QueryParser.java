package com.datametica.queryexecuter.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IQueryParser;
import com.datametica.queryexecuter.util.Util;

@Service
public class QueryParser implements IQueryParser {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final String QUERY_STR = "query-str";

	@Override
	public String getQuery(QueryConfig queryConfig) {
		Map<String, String> queryProperties = Util.getPropertyDetailsAsMap(queryConfig.getQuery()
				.getIQueryDetailsList());
		String queryStr = queryProperties.get(QueryParser.QUERY_STR);
		if(!StringUtils.isEmpty(queryConfig.getFilter())){
			queryStr = queryStr.replaceAll("1\\s*=\\s*1", queryConfig.getFilter());
		}
		logger.info(String.format("Generated query string:%s", queryStr));
		return queryStr;
	}
}

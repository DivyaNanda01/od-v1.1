package com.datametica.queryexecuter.service;


/**
 * @author mahendra.tonape
 *
 */
public interface IQueryDetails extends IPropertyDetail{
	Integer getId();
	IQuery getQuery();
}

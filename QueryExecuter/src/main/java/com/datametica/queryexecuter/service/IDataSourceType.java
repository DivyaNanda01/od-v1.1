package com.datametica.queryexecuter.service;

import java.util.List;

public interface IDataSourceType {
	Integer getDataSourceTypeId();
	String getName();
	
	List<IDataSource> getIDataSourceList();
	
}

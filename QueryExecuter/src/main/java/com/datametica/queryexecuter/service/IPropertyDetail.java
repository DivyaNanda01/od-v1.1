package com.datametica.queryexecuter.service;

public interface IPropertyDetail {
	public String getPropertyName();
	public String getPropertyValue();
}

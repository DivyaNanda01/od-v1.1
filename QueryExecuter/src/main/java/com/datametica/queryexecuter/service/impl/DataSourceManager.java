package com.datametica.queryexecuter.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.datametica.queryexecuter.service.IDataSource;
import com.datametica.queryexecuter.service.IDataSourceManager;
import com.datametica.queryexecuter.util.Util;

@Service
public class DataSourceManager implements IDataSourceManager {

	private static final String JNDI_RESOURCE = "jndi-resource";
	private static final String URL = "url";
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String DRIVER_CLASS = "driver-class";
	
	private Map<String, DataSource> dsCache = new HashMap<>();
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public DataSource getDataSource(
			IDataSource dataSource,
			Map<String, String> userDetails){
		DataSource sqlDS = dsCache.get(dataSource.getName());
		
		if(sqlDS == null){
			Map<String, String> dataSourceAttrs = Util.getPropertyDetailsAsMap(
					dataSource.getIDataSourceConnectionDetails());
			if(!dataSourceAttrs.isEmpty()){
				if(dataSource.getName().startsWith(DataSourceManager.JNDI_RESOURCE)){
					String jndiResourceName = dataSourceAttrs.get(DataSourceManager.JNDI_RESOURCE);
					
					if(jndiResourceName == null) {
						String error = String.format("For JNDI datasource, %s should be provided", 
								DataSourceManager.JNDI_RESOURCE);
						logger.error(error);
						throw new IllegalArgumentException(error);
					}
					
					try {
						Context context = new InitialContext();
						logger.info(String.format("Looking up data source %s using JNDI context", 
								jndiResourceName));
						sqlDS = (DataSource) context.lookup(jndiResourceName);
					} catch (NamingException e) {
						logger.error(
								String.format("Error in looking up data source %s", 
								jndiResourceName), e);
						throw new RuntimeException(e);
					}finally{
						if(sqlDS != null){
							logger.info(String.format("Datasource %s found", 
									jndiResourceName));
						}else{
							logger.debug(String.format("Datasource %s not found", 
									jndiResourceName));							
						}
					}
				}else{
					String driverClass = dataSourceAttrs.get(DataSourceManager.DRIVER_CLASS);
					String url = dataSourceAttrs.get(DataSourceManager.URL);
					String userName = dataSourceAttrs.get(DataSourceManager.USERNAME);
					String password = dataSourceAttrs.get(DataSourceManager.PASSWORD);
					
					if(driverClass != null && url != null && userName != null && password != null){
						try {
							Class.forName(driverClass, false, getClass().getClassLoader());
						} catch (ClassNotFoundException e) {
							logger.error(String.format("Driver class %s not found!", driverClass));
							throw new IllegalArgumentException(e);
						}
						BasicDataSource ds = new BasicDataSource();
						ds.setDriverClassName(driverClass);
						ds.setUrl(url);
						ds.setUsername(userName);
						ds.setPassword(password);
						logger.info(String.format(
								"Created data source for URL:%s, driver class:%s",
								url, 
								driverClass));
						sqlDS = ds;
					}else{
						logger.error("All connection properties are not provided");
						throw new IllegalArgumentException("Driver class, URL, user name and" +
									" password must be provided" );

					}
				}
			}else{
				logger.error("Datasource creation failed due to missing connection params");
				throw new IllegalArgumentException("Missing connection params..");
			}
		
			if(sqlDS != null) dsCache.put(dataSource.getName(), sqlDS);
		}

		return sqlDS;		
	}
}

package com.datametica.queryexecuter.service;

public interface IDataSourceCreator {
	public IDataSource getDataSource(IDataSourceConfig config);
}

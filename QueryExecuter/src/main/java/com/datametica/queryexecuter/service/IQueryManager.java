package com.datametica.queryexecuter.service;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.datametica.queryexecuter.model.QueryConfig;

/**
 * @author mahendra.tonape
 *
 */
public interface IQueryManager {
	
	 /**
	 * Runs the query based on query configuration provided and returns the result.
	 * @param QueryConfig
	 * @return
	 */
	List<Map<String,Object>> runQuery(QueryConfig QueryConfig);
	String getQuery(QueryConfig queryConfig);
	DataSource getDatasource(QueryConfig queryConfig);

}

package com.datametica.queryexecuter.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datametica.queryexecuter.model.QueryConfig;
import com.datametica.queryexecuter.service.IQueryManager;
import com.datametica.queryexecuter.service.IQueryService;

@Service
public class QueryService implements IQueryService {

	@Autowired
	private IQueryManager queryManager;

	/**
	 * Returns query as per supplied query configuration params and 
	 * data source params. 
	 */
	@Override
	public List<Map<String, Object>> getData(QueryConfig queryConfig) {
		return queryManager.runQuery(queryConfig);
	}

	public void setQueryManager(IQueryManager queryManager) {
		this.queryManager = queryManager;
	}

}
